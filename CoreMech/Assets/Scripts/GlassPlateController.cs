﻿using UniRx;
using UnityEngine;

public class GlassPlateController : MonoBehaviour
{
    [SerializeField] private bool _isRightAnswer;
    [SerializeField] private Answer _plateData;
    [SerializeField] private float _delayTime = 3.5f;
    public float DelayTime { get => _delayTime; set => _delayTime = value; }
    public Transform _target;
    [Header("Prefabs")]
    [SerializeField] private GameObject _plateCtrl;
    [SerializeField] private GameObject _wonCondition;
    [SerializeField] private ResultPlateController _brokenPlate;
    [SerializeField] private GlassPlateCanvasController _canvas;
    private CompositeDisposable disposables;
    private GlassPlatePairController pairController;

    public GlassPlatePairController PairController
    {
        get
        {
            if (pairController == null)
                pairController = GetComponentInParent<GlassPlatePairController>();
            return pairController;
        }
    }

    public void ShowAnswer()
    {
        _canvas.Init(_plateData);
    }

    public void HideAnswer()
    {
        _canvas.Hide(true);
    }

    public void Init(Answer answer, bool _right)
    {
        _plateData = answer;
        _isRightAnswer = _right;
    }

    public bool DoCheck(bool isPlayer = true)
    {
        if (_isRightAnswer)
        {
            if (isPlayer)
            {
                _wonCondition.gameObject.SetActive(true);
                Observable.Timer(System.TimeSpan.FromSeconds(_delayTime)).Subscribe(_ =>
                {
                    if (GameManager.Instance.IsPlayerAlive())
                        PairController.DisplayNextQuestion();
                }).AddTo(disposables);
            }
            return true;
        }
        else
        {
            _plateCtrl.gameObject.SetActive(false);
            _brokenPlate.DoBroken();
            _brokenPlate.gameObject.SetActive(true);
            return false;
        }
    }

    void OnEnable()
    {
        disposables = new CompositeDisposable();
    }

    void OnDisable()
    {
        if (disposables != null)
        {
            disposables.Dispose();
        }
    }
}