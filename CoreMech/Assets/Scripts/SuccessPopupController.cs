﻿using UnityEngine;
using UnityEngine.UI;

public class SuccessPopupController : BasePopupController
{
    public static SuccessPopupController Instance;
    [SerializeField] private SkinItemController _skinItemController;
    [SerializeField] private Button _applyButton;
    private Skin _skin;

    public void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
        _applyButton.onClick.AddListener(() => { _tweenPopupController.Hide(false); });
    }

    /// <summary>
    /// For SuccesPopup send Skin
    /// </summary>
    /// <param name="args"></param>
    public override void Init(params object[] args)
    {
        _skin = args[0] as Skin;
        _skinItemController.Init(_skin, true, false);
        base.Init();
    }
}
