﻿using UnityEngine;

public class BloodStainMaker : MonoBehaviour
{
    [SerializeField] private SpritePool _sprites;
    [SerializeField] private Vector3 _randomOffset = new Vector3(0.2f, 0, 0.2f);
    [SerializeField] private Vector3 _randomScale = new Vector3(0.25f, 0, 0.25f);
    [SerializeField] private SpriteRenderer _bloodItemPrefab;
    [SerializeField] private SoundTrigger _soundTrigger;

    private SpriteRenderer _lastItem;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Floor")
        {
            if (_lastItem != null && LevelBuilder.RemoveGeneratedObject(_lastItem.gameObject))
            {
                Destroy(_lastItem);
                _lastItem = null;
            }
            if (_soundTrigger != null)
                _soundTrigger.PlayRandom();
            var pos = transform.position;
            pos = new Vector3(pos.x + Random.Range(-_randomOffset.x, _randomOffset.x), -4.495f, pos.z + Random.Range(-_randomOffset.z, _randomOffset.z));
            var rot = Quaternion.Euler(90, 0, 0);
            var blood = Instantiate(_bloodItemPrefab, pos, rot);
            blood.transform.localScale = Vector3.one * _randomScale.x + new Vector3(Random.Range(-_randomScale.x, _randomScale.x), 0.25f, Random.Range(-_randomScale.z, _randomScale.z));
            blood.sprite = _sprites._spritePool[Random.Range(0, _sprites._spritePool.Count)].sprite;
            _lastItem = blood;
            LevelBuilder.AddGeneratedObject(blood.gameObject);
        }
    }
}
