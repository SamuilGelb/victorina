﻿using TMPro;
using UnityEngine;

public class TweenFontSizeFlash : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    private TextMeshProUGUI Text
    {
        get
        {
            if (_text == null)
                _text = GetComponent<TextMeshProUGUI>();
            return _text;
        }
    }
    [SerializeField] private bool _activeOnEnable = false;
    [SerializeField] private float _minValue = 0.5f;
    [SerializeField] private float _maxValue = 1.5f;
    [SerializeField] private float _loopTime = 1f;
    [SerializeField] private bool _loop;

    public void OnEnable()
    {
        if (_activeOnEnable)
            Flash();
    }

    private void Flash()
    {
        LeanTween.value(Text.fontSize, _minValue, _loopTime).setOnUpdate((float x) => { Text.fontSize = x; }).setOnComplete(() =>
        {
            LeanTween.value(Text.fontSize, _maxValue, _loopTime).setOnUpdate((float x) => { Text.fontSize = x; }).setOnComplete(() =>
            {
                if (_loop)
                    Flash();
            });
        });
    }
}
