﻿using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class EarnMoneyUIController : MonoBehaviour
{
    public RectTransform _transform { get => transform as RectTransform; }
    [Header("Links")]
    [SerializeField] private ParticleSystem _coinSource;
    [SerializeField] private Image _coinFills;
    [SerializeField] private Image _coinsBunch;
    [SerializeField] private TextMeshPro _coinsCount;
    private CompositeDisposable disposables;
    #region Constants
    [Header("Constants")]
    private const float BASE_HEIGHT = 500;
    private const float DISPLAY_HEIGHT = -100;
    private const float BUNCH_DOWN_POINT = 65;
    private const float BUNCH_TOP_POINT = 355;
    private const float GROW_DELAY = 1.6f;
    private const int TOP_VALUE = 1000;
    #endregion

    private void Awake()
    {
        Show(300, 450);
    }

    public void Show(int previousValue, int income)
    {
        _coinSource.gameObject.SetActive(false);
        LeanTween.value(BASE_HEIGHT, DISPLAY_HEIGHT - 50, 1f).setOnUpdate(
            (float v) =>
            {
                _transform.anchoredPosition = new Vector2(0, v);
            });
        LeanTween.value(DISPLAY_HEIGHT - 50, DISPLAY_HEIGHT, 0.2f)
            .setDelay(1f).setOnUpdate((float v) =>
            {
                _transform.anchoredPosition = new Vector2(0, v);
            }).setOnComplete(
            _ =>
            {
                _coinSource.gameObject.SetActive(true);
            });
        var val = previousValue % TOP_VALUE;
        _coinFills.fillAmount = (float)val / (float)TOP_VALUE;
        _coinsBunch.rectTransform.anchoredPosition = new Vector2(0,
            (BUNCH_TOP_POINT - BUNCH_DOWN_POINT) * _coinFills.fillAmount + BUNCH_DOWN_POINT);
        LeanTween.value(_coinFills.fillAmount, (((float)val + (float)income) / (float)TOP_VALUE), 2f).
            setDelay(1.2f + GROW_DELAY).setOnUpdate((float v) =>
            {
                _coinFills.fillAmount = v;
                _coinsBunch.rectTransform.anchoredPosition = new Vector2(0,
            (BUNCH_TOP_POINT - BUNCH_DOWN_POINT) * v + BUNCH_DOWN_POINT);
            }).setOnComplete(
            _ =>
            {
                _coinSource.gameObject.SetActive(false);
                Hide();
            });
    }

    private void Hide()
    {
        LeanTween.value(DISPLAY_HEIGHT, DISPLAY_HEIGHT - 50, 0.2f)
            .setOnUpdate((float v) =>
            {
                _transform.anchoredPosition = new Vector2(0, v);
            });
        LeanTween.value(DISPLAY_HEIGHT - 50, BASE_HEIGHT, 1f)
            .setDelay(0.2f).setOnUpdate(
            (float v) =>
            {
                _transform.anchoredPosition = new Vector2(0, v);
            });
    }

    void OnEnable()
    {
        disposables = new CompositeDisposable();
    }

    void OnDisable()
    {
        if (disposables != null)
        {
            disposables.Dispose();
        }
    }
}
