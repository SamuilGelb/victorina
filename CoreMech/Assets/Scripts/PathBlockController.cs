﻿using UnityEngine;

public class PathBlockController : MonoBehaviour
{
    [SerializeField] private Question _questionData;

    public void Init(Question questionData)
    {
        _questionData = questionData;
    }
}