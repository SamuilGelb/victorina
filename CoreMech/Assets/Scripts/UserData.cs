﻿using System;

[Serializable]
public class UserData 
{
    public string Username;
    public int CurrentLevel;
    public int Coins;
    public int Reputation;
    public int Score;
    public string[] Skins;
    public string ActiveSkin;
    public bool Sound;
    public bool Music;
    public bool AdsBuyed;

    public UserData()
    {
        Username = $"User #{new Random(DateTime.Now.Second).Next(0, 47483647)}";
        CurrentLevel = 1;
        Coins = 0;
        Reputation = 0;
        Score = 0;
        Skins = new string[] { "Default" };
        ActiveSkin = "Default";
        Sound = true;
        Music = true;
        AdsBuyed = true;
    }

    internal void Reset()
    {
        CurrentLevel = 0;
        Coins = 0;
        Reputation = 0;
        Score = 0;
        Skins = new string[0];
    }
}
