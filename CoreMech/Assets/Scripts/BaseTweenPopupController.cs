﻿using UnityEngine;

public class BaseTweenPopupController : MonoBehaviour
{
    [SerializeField] private RectTransform _mainPanel;
    [SerializeField] private RectTransform _popupItem;
    public delegate void ViewCompleate();
    public event ViewCompleate OnShowCompleate;
    public event ViewCompleate OnHideCompleate;

    protected RectTransform MainPanel
    {
        get
        {
            if (_mainPanel == null)
                _mainPanel = transform.GetChild(0) as RectTransform;
            return _mainPanel;
        }
    }

    protected RectTransform PopupItem
    {
        get
        {
            if (_popupItem == null)
                _popupItem = MainPanel.GetChild(0) as RectTransform;
            return _popupItem;
        }
    }

    public bool IsActive
    {
        get => MainPanel.gameObject.activeInHierarchy;
    }

    public virtual void Show()
    {
        _mainPanel.gameObject.SetActive(true);
        _popupItem.anchoredPosition = Vector2.zero;
    }

    public virtual void Hide(bool fast = false)
    {
        if (fast)
        {
            _mainPanel.gameObject.SetActive(false);
            _popupItem.anchoredPosition = new Vector2(0, Screen.height);
        }
    }

    protected void OnViewChanged(bool show)
    {
        if (show)
        {
            if (OnShowCompleate != null)
                OnShowCompleate();
        }
        else
        {
            if (OnHideCompleate != null)
                OnHideCompleate();
        }
    }
}