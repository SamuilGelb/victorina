﻿using UnityEngine;
using UnityEngine.UI;

public class LoseCanvasController : BaseCanvasController
{
    [Header("Linked")]
    [SerializeField] private Image _panel;
    [SerializeField] private RectTransform _popup;
    [SerializeField] private Button _repeatButton;
    [SerializeField] private Button _quitButton;

    private void OnEnable()
    {
        _quitButton.onClick.AddListener(() => { GameManager.Instance.BackToMenu(); Hide(false); });
        _repeatButton.onClick.AddListener(() => { GameManager.Instance.LoadLevel(LevelToLoad.Current); Hide(false); });

    }

    private void OnDisable()
    {
        _quitButton.onClick.RemoveAllListeners();
        _repeatButton.onClick.RemoveAllListeners();
    }

    public override void Show()
    {
        base.Show();
        _popup.anchoredPosition = new Vector2(0, -Screen.height);
        LeanTween.color(_panel.rectTransform, new Color(0, 0, 0, 0.5f), 0.5f).setOnUpdate(
            (Color col) =>
            {
                _panel.color = col;
            });
        LeanTween.value(-Screen.height, 100, 1f).setDelay(0.5f).setOnUpdate(
            (float v) =>
            {
                _popup.anchoredPosition = new Vector2(0, v);
            });
        LeanTween.value(100, 0, 0.2f).setDelay(1.5f).setOnUpdate(
            (float v) =>
            {
                _popup.anchoredPosition = new Vector2(0, v);
            });
    }

    public override void Hide(bool fast = false)
    {
        base.Hide(fast);
        LeanTween.value(0, 100, 0.2f).setOnUpdate(
            (float v) =>
            {
                _popup.anchoredPosition = new Vector2(0, v);
            });
        LeanTween.value(100, -Screen.height, 1f).setDelay(0.2f).setOnUpdate(
            (float v) =>
            {
                _popup.anchoredPosition = new Vector2(0, v);
            });
        LeanTween.color(_panel.rectTransform, new Color(0, 0, 0, 0.0f), 0.5f).setDelay(1.2f).setOnUpdate(
            (Color col) =>
            {
                _panel.color = col;
            });
    }
}
