﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class MainMenuCanvasController : BaseCanvasController
{
    public Button _playButton;
    public GameStarter _gameStarter;

    public TextMeshProUGUI _coinsCount;
    public TextMeshProUGUI _welcomeText;
    public string welcome = "<size=60>Welcome</size>\n\n";
    public Button _adsButton;
    public TextMeshProUGUI _dayNum;

    [SerializeField] private bool _settingsOpen;
    [SerializeField] private Button _settingsButton;
    [SerializeField] private RectTransform _settings;
    [SerializeField] private List<Sprite> _musicButtons;
    [SerializeField] private Button _soundButton;
    [SerializeField] private Button _musicButton;
    [SerializeField] private List<Sprite> _soundButtons;
    [SerializeField] private RectTransform _displayUsername;
    [SerializeField] private TextMeshProUGUI _userName;
    [SerializeField] private Button _editUserName;
    [SerializeField] private RectTransform _editUser;
    [SerializeField] private TMP_InputField _userNameField;
    [SerializeField] private Button _okButton;
    [SerializeField] private Button _closeButton;
    [SerializeField] private Button _skinsButton;
    [SerializeField] public SkinSelectCanvasController _skinSelector;

    public new void Awake()
    {
        base.Awake();
        _playButton.onClick.AddListener(() => StartGame());
        _adsButton.onClick.AddListener(() => BuyAds());
        _settingsButton.onClick.AddListener(() => Settings());
        _soundButton.onClick.AddListener(() => OnSound());
        _musicButton.onClick.AddListener(() => OnMusic());
        _editUserName.onClick.AddListener(() => EditUserName(true));
        _okButton.onClick.AddListener(() => EditUserName(false));
        _closeButton.onClick.AddListener(() => SaveSettings());
        _userNameField.onEndEdit.AddListener((string v) => ChangeName(v));
        _skinsButton.onClick.AddListener(() => { OpenSkinWindow(); });
        GameManager.OnUserDataChanged += UpdateView;
    }

    private void OpenSkinWindow()
    {
        _skinSelector.Init();
    }

    private void UpdateView()
    {
        _userName.text = GameManager.Instance.UserData.Username;
        _coinsCount.text = $"{GameManager.Instance.UserData.Coins}";
        _welcomeText.text = $"{welcome}{GameManager.Instance.UserData.Username}";
}

    private void Settings()
    {
        _settings.gameObject.SetActive(!_settings.gameObject.activeInHierarchy);
    }

    private void ChangeName(string v)
    {
        GameManager.Instance.UserData.Username = v;
        _userName.text = v;
        _welcomeText.text = $"{welcome}{v}";
        DataModelGetter.SaveUserData(GameManager.Instance.UserData);
    }

    private void SaveSettings()
    {
        DataModelGetter.SaveUserData(GameManager.Instance.UserData);
        _settings.gameObject.SetActive(false);
    }

    private void EditUserName(bool v)
    {
        _editUser.gameObject.SetActive(v);
        _displayUsername.gameObject.SetActive(!v);
        GameManager.Instance.UserData.Username = _userNameField.text;
    }

    private void OnMusic()
    {
        GameManager.Instance.UserData.Music = !GameManager.Instance.UserData.Music;
        _musicButton.image.sprite = GameManager.Instance.UserData.Music ? _musicButtons[0] : _musicButtons[1];
        DataModelGetter.SaveUserData(GameManager.Instance.UserData);
        SoundMaster.Instance._musicSource.mute = !GameManager.Instance.UserData.Music;
    }

    private void OnSound()
    {
        GameManager.Instance.UserData.Sound = !GameManager.Instance.UserData.Sound;
        _soundButton.image.sprite = GameManager.Instance.UserData.Sound ? _soundButtons[0] : _soundButtons[1];
        DataModelGetter.SaveUserData(GameManager.Instance.UserData);
    }

    private void BuyAds()
    {
        Debug.Log("HideAds");
    }

    private void StartGame()
    {
        _gameStarter.StartGame();
        Hide(true);
    }

    public void Init()
    {
        UserData data = GameManager.Instance.UserData;
        _settings.gameObject.SetActive(false);
        _coinsCount.text = $"{data.Coins}";
        _welcomeText.text = $"{welcome}{data.Username}";
        _adsButton.gameObject.SetActive(!data.AdsBuyed);
        _dayNum.text = $"Day {data.CurrentLevel}";
        _soundButton.image.sprite = data.Sound ? _soundButtons[0] : _soundButtons[1];
        _musicButton.image.sprite = data.Music ? _musicButtons[0] : _musicButtons[1];
        _displayUsername.gameObject.SetActive(true);
        _userName.text = data.Username;
        _editUser.gameObject.SetActive(false);
        _userNameField.text = data.Username;
    }
}
