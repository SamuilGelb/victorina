﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(RagdollController))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private RagdollController _ragdollController;
    [SerializeField] private Rigidbody _rigidbody;
    #region props
    public Animator Animator
    {
        get
        {
            if (_animator == null)
                _animator = GetComponent<Animator>();
            return _animator;
        }
    }
    public RagdollController RagdollController
    {
        get
        {
            if (_ragdollController == null)
                _ragdollController = GetComponent<RagdollController>();
            return _ragdollController;
        }
    }
    public new Rigidbody rigidbody
    {
        get
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody>();
            return _rigidbody;
        }
    }

    #endregion
    [Header("View")]
    [SerializeField] private SkinnedMeshRenderer _meshRenderer;
    [Header("Setup")]
    [SerializeField] private Vector3 _initialPosition = new Vector3(0, 5, 0);
    [SerializeField] private Transform _defaultTarget;
    [Header("CharMoveData")]
    [SerializeField] private Transform _target;
    [SerializeField] private float _rotationSpeed = 3;
    [SerializeField] private float _moveSpeed = 4;
    [SerializeField] private float _jumpForce = 10;
    [SerializeField] private bool _jump = false;
    [SerializeField] float m_GroundCheckDistance = 0.1f;
    [SerializeField] bool m_IsGrounded;
    [SerializeField] LayerMask layerMask;
    [Header("BotSetup")]
    [SerializeField] private List<NumController> numsControll = new List<NumController>();
    [SerializeField] private bool _isBot = false;
    [SerializeField] private Vector3 _offset = new Vector3();
    public Vector3 Offset { get => _offset; }

    public void SetNum(List<int> nums)
    {
        int c = numsControll.Count;
        for (int i = 0; i < c; i++)
        {
            numsControll[i].SetNum(nums[i]);
        }
    }

    public void Awake()
    {
        _offset = new Vector3(Random.Range(-13f, 13f), 0, Random.Range(-13f, 13f));
        if (!_isBot)
            SetNum(new List<int>() { 0, 0, 1 });
    }

    internal void SetSkin(Skin skinValue)
    {
        _meshRenderer.material = skinValue._skinMat;
    }

    public void ChangeTarget(Transform target)
    {
        _target = target;
    }

    public void Die()
    {
        Animator.enabled = false;
        this.enabled = false;
        RagdollController.ActivateRagdoll();
    }

    internal void ResetPosition()
    {
        this.enabled = true;
        _target = _defaultTarget;
        transform.position = _initialPosition;
        RagdollController.DeactivateRagdoll();
        Animator.enabled = true;
    }

    internal void ChangeOffset(float mult)
    {
        var val = new Vector3(Random.Range(-13f, 13f), 0, Random.Range(-8f, 0)) * mult;
        _offset = val;
    }

    private void MoveToPoint()
    {
        Vector3 currentPos = new Vector3(transform.position.x, 5, transform.position.z);
        Vector3 targetPos = new Vector3(_target.position.x, 5, _target.position.z);
        if (_isBot)
            targetPos += _offset;
        Vector3 velocity = (transform.forward * _moveSpeed) + new Vector3(0, rigidbody.velocity.y, 0);
        //Vector3 jumpCheckDirection = transform.forward + new Vector3(0, -1, 0);
        //Debug.DrawRay(transform.position + (Vector3.up * 0.1f), transform.forward + (transform.up * -1), Color.green, 5f);
        if (CheckGroundStatus())
        {
            RaycastHit hit;
            if (!Physics.Raycast(transform.position + (Vector3.up * 0.1f), transform.forward + (transform.up * -1), out hit, 9f, layerMask.value))
            {
                var jumpLeg = Random.Range(-0.75f, 0.75f);
                _animator.SetFloat("JumpLeg", jumpLeg);
                _jump = true;
            }
        }
        if (!m_IsGrounded)
        {
            _animator.SetFloat("Jump", _rigidbody.velocity.y);
            if (Mathf.Abs(_rigidbody.velocity.y) < 0.2f)
            {
                _animator.SetBool("OnGround", true);
            }
        }
        if (_jump)
        {
            if (velocity.y <= _jumpForce)
                velocity = new Vector3(velocity.x, _jumpForce, velocity.y);
            //velocity += Vector3.up * _jumpForce;
            _jump = false;
        }
        if (Vector3.Distance(currentPos, targetPos) > .6f)
        {
            rigidbody.velocity = velocity/*transform.forward * _moveSpeed*/;
            _animator.SetFloat("Forward", 1);
        }
        else
        {
            if (Vector3.Distance(currentPos, targetPos) < 0.5f)
            {
                rigidbody.velocity = Vector3.zero + new Vector3(0, rigidbody.velocity.y, 0);
                _animator.SetFloat("Forward", 0);
            }
        }
        var prevRot = transform.rotation;
        var rotation = Quaternion.Slerp(transform.localRotation, Quaternion.LookRotation((Vector3.Distance(currentPos, targetPos) > 1f ? targetPos : transform.position + new Vector3(0, 0, 10)) - transform.position), Time.deltaTime * _rotationSpeed);
        transform.rotation = Quaternion.Euler(0, rotation.eulerAngles.y, 0);
        if (Mathf.Abs(prevRot.eulerAngles.y - rotation.eulerAngles.y) > 0.5f)
        {
            _animator.SetFloat("Turn", prevRot.eulerAngles.y > rotation.eulerAngles.y ? -1 : 1);
        }
        else
        {
            _animator.SetFloat("Turn", 0);
        }
    }

    bool CheckGroundStatus()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
        {
            m_IsGrounded = true;
            _animator.applyRootMotion = true;
        }
        else
        {
            m_IsGrounded = false;
            _animator.applyRootMotion = false;
        }
        _animator.SetBool("OnGround", m_IsGrounded);
        return m_IsGrounded;
    }

    private void FixedUpdate()
    {
        if (_target == null) return;
        MoveToPoint();
    }
}
