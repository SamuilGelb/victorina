﻿using TMPro;
using System;
using UnityEngine.UI;

public class MainCanvasControll : BaseCanvasController
{
    public TextMeshProUGUI _gameNum;
    public Button _backButton;
    private Action<bool> onAnswered;

    public new void Awake()
    {
        base.Awake();
        onAnswered = OnPopupAnswer;
        _backButton.onClick.AddListener(OnBackClick);
        GameManager.OnUserDataChanged += InitView;
    }

    private void OnBackClick()
    {
        SurePopupContent.Instance.Init(onAnswered);
    }

    public void OnPopupAnswer(bool value)
    {
        if(value)
        {
            GameManager.Instance.Defeat();
        }
    }

    public void InitView()
    {
        _gameNum.text = $"Day {GameManager.Instance.CurrentLevelData.Num}";
    }
}
