﻿using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private DataModel _currentLevelData;
    public DataModel CurrentLevelData { get => _currentLevelData; }
    [Header("Controllers")]
    [SerializeField] private LevelBuilder _levelBuilder;
    [SerializeField] private EnvironmentController _environmentController;
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private BotsController _botsController;
    [SerializeField] private UIQuestController _uIQuestController;
    [SerializeField] private DefeatController _defeatController;
    [SerializeField] private WinController _winController;
    [SerializeField] private MainCanvasControll _mainCanvas;
    [SerializeField] private MainMenuCanvasController _mainMenu;
    [SerializeField] private GameStarter _gameStarter;
    [SerializeField] private UserData _userData;
    public PlayerController Player { get => _playerController; }
    public UserData UserData { get => _userData; }

    public delegate void UserDataChanged();
    public static event UserDataChanged OnUserDataChanged;
    private GlassPlatePairController _nextGlassPair;
    public static GameManager Instance;

    internal void QuitGame()
    {
        Application.Quit();
    }

    internal void BackToMenu()
    {
        UnityAds.Instance._wached = true;
        ResetLevel();
        _gameStarter.PauseGame();
        _uIQuestController.Hide(true);
        _mainMenu.Show();
        _mainCanvas.Hide(true);
        _mainMenu.Init();
    }

    internal void Defeat()
    {
        _defeatController.Die(false);
    }

    internal void ApplyReward()
    {
        _userData.Coins += _currentLevelData.Reward.Money;
        _userData.CurrentLevel = _currentLevelData.Num + 1;
        if (string.IsNullOrEmpty(_currentLevelData.Reward.Skin))
            AddSkin(_currentLevelData.Reward.Skin);
        DataModelGetter.SaveUserData(_userData);
        OnUserDataChanged();
    }

    internal void ApplyReward(Reward reward)
    {
        _userData.Coins += reward.Money;
        if (!string.IsNullOrEmpty(reward.Skin))
            AddSkin(reward.Skin);
        OnUserDataChanged();
    }

    public bool IsPlayerAlive()
    {
        return _playerController.enabled;
    }

    internal async void LoadLevel(LevelToLoad level)
    {
        switch (level)
        {
            case LevelToLoad.Previous:
                _currentLevelData = await DataModelGetter.GetData($"level_{_currentLevelData.Num - 1}");
                ResetLevel();
                break;
            case LevelToLoad.Current:
                ResetLevel();
                break;
            case LevelToLoad.Next:
                _currentLevelData = await DataModelGetter.GetData($"level_{_currentLevelData.Num + 1}");
                ResetLevel();
                break;
            default:
                ResetLevel();
                break;
        }
        OnUserDataChanged();
    }

    internal void AddSkin(string name)
    {
        var skins = _userData.Skins.ToList();
        skins.Add(name);
        _userData.Skins = skins.ToArray();
        DataModelGetter.SaveUserData(_userData);
        OnUserDataChanged();
    }

    internal void ActivateSkin(Skin skinValue)
    {
        _playerController.SetSkin(skinValue);
        _userData.ActiveSkin = skinValue._name;
        DataModelGetter.SaveUserData(_userData);
        OnUserDataChanged();
    }

    private void ResetLevel()
    {
        UnityAds.Instance.LoadAdinterstitial();
        _botsController.ClearBots();
        _levelBuilder.ClearLevel();
        _playerController.ResetPosition();
        _defeatController.Reset();
        _winController.Reset();
        //_userData.Reset();
        _mainCanvas.InitView();
        _levelBuilder.GenerateLevel(_currentLevelData);
        _botsController.SpawnBots();
        OnUserDataChanged();
    }


    public Transform GetFinalPoint()
    {
        return _winController._finish;
    }

    public void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        StartGetinData();
    }

    private async void StartGetinData()
    {
        LoadCanvas.Instance.InitMaxValue(4);
        _userData = await DataModelGetter.GetUserData();
        SoundMaster.Instance._musicSource.mute = !_userData.Music;
        LoadCanvas.Instance.ChangeValue(1);
        if (_userData.CurrentLevel > 0)
            _currentLevelData = await DataModelGetter.GetData($"level_{_userData.CurrentLevel}");
        LoadCanvas.Instance.ChangeValue(1);
        InitLevel(_userData.CurrentLevel);
        await System.Threading.Tasks.Task.Delay(200);
        LoadCanvas.Instance.ChangeValue(1);
        _mainMenu.Init();
        await System.Threading.Tasks.Task.Delay(200);
        LoadCanvas.Instance.ChangeValue(1);
        OnUserDataChanged();
    }


    public void DisplayQuestion(Question question, GlassPlatePairController _glassPair)
    {
        _nextGlassPair = _glassPair;
        _uIQuestController.DisplayQuest(question, OnSelect);
        _glassPair.DisplayAnswers();
    }

    private void OnSelect(Side side)
    {
        _nextGlassPair.HideAnswers();
        _botsController.ChooseAction(_nextGlassPair, side);
        _playerController.ChangeTarget(_nextGlassPair.GetNextPoint(side));
    }

    public void GoToWinPoint()
    {
        _playerController.ChangeTarget(GetFinalPoint());
    }

    public void InitLevel(int currentLevel)
    {
        //_currentLevelData = new DataModel();
        var size = _levelBuilder.GenerateLevel(_currentLevelData);
        _environmentController.Init(size);
        _botsController.SpawnBots(true);
    }

}

public enum LevelToLoad
{
    Previous,
    Current,
    Next
}