﻿using TMPro;
using UnityEngine.UI;

public class GlassPlateCanvasController : BaseCanvasController
{
    public TextMeshProUGUI _answerText;
    public Image _answerImage;

    public void Init(Answer plateData)
    {
        _answerImage.gameObject.SetActive(false);
        var imageSource = DataModelUtils.ContainImage(plateData.answer);
        if (imageSource != ImageSource.Null)
        {
            _answerImage.gameObject.SetActive(true);
            switch (imageSource)
            {
                case ImageSource.Link:
                    StartCoroutine(DataModelUtils.DownloadImage(plateData.answer.ImageURL, _answerImage));
                    break;
                case ImageSource.Resources:
                    _answerImage.sprite = DataModelUtils.GetImage(plateData.answer.ImageName);
                    break;
            }
        }
        _answerText.text = plateData.answer.Text;
        Show();
    }
}
