﻿using UnityEngine;

public class ResultPlateController : MonoBehaviour
{
    [SerializeField] private float _destroyDelay = 10f;

    public virtual void DoBroken()
    {
        Destroy(gameObject, _destroyDelay);
    }

    public virtual void DoRight()
    {

    }

    public virtual void StartChoise()
    {

    }
}
