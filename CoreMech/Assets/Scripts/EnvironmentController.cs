﻿using UnityEngine;

public class EnvironmentController : MonoBehaviour
{
    [SerializeField] private Transform _water;

    public void Init(Vector3 size)
    {
        _water.localScale = new Vector3(size.x * 3, 1, size.z * 3);
        _water.position = new Vector3(0, -5, size.z / 2);
    }
}
