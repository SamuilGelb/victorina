﻿using UnityEngine;

public class SoundMaster : MonoBehaviour
{
    public static SoundMaster Instance;

    public AudioSource _musicSource;

    public void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);
    }
}
