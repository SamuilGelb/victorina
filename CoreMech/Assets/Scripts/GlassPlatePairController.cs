﻿using UnityEngine;

public class GlassPlatePairController : MonoBehaviour
{
    [SerializeField] private GlassPlateController _right;
    [SerializeField] private GlassPlateController _left;
    public GlassPlatePairController _nextPair;
    public Question _question;
    private bool _last;

    public Transform GetNextPoint(Side side)
    {
        switch (side)
        {
            case Side.Left:
                return _left._target;
            case Side.Right:
                return _right._target;
            default:
                return _right._target;
        }
    }

    public void DisplayNextQuestion()
    {
        if (!_last)
        {
            GameManager.Instance.DisplayQuestion(_question, _nextPair);
        }
        else
        {
            GameManager.Instance.GoToWinPoint();
        }
    }

    public void DisplayAnswers()
    {
        _right.ShowAnswer();
        _left.ShowAnswer();
    }

    public void Init(Question questionToIt, Question nextQuestion, GlassPlatePairController next)
    {
        _question = nextQuestion;
        int side = Random.Range(0, 2);
        if (side == 0)
        {
            _right.Init(questionToIt.RightAnswer, true);
            _left.Init(questionToIt.WrongAnswer, false);
        }
        else
        {
            _right.Init(questionToIt.WrongAnswer, false);
            _left.Init(questionToIt.RightAnswer, true);
        }
        if (next != null)
            _nextPair = next;
        else
        {
            _left.DelayTime = 1.5f;
            _last = true;
        }

    }

    internal void HideAnswers()
    {
        _right.HideAnswer();
        _left.HideAnswer();
    }
}
