﻿using System;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Camera _camera;

    public Camera Camera
    {
        get
        {
            if (_camera == null)
                _camera = GetComponent<Camera>();
            return _camera;
        }
    }

    [SerializeField] private Transform _target;
    [SerializeField] private Vector3 _startPosition;
    [SerializeField] private Vector3 _startAngle;
    [SerializeField] private float _startView = 90f;
    [SerializeField] private Vector3 _playDistance = new Vector3(0, 10f, -3f);
    [SerializeField] private Vector3 _playAngle = new Vector3(50, 0, 0);
    [SerializeField] private float _playView = 75f;
    [SerializeField] public bool _follow = true;
    [SerializeField] private float _followSpeed = 1f;
    public float _height = 0f;

    public void FixedUpdate()
    {
        if (!_follow) return;
        this.Camera.fieldOfView = Mathf.Lerp(this.Camera.fieldOfView, _playView, Time.deltaTime / 2);
        transform.position = Vector3.Lerp(transform.position,
            new Vector3(_playDistance.x,
                _playDistance.y + _height,
                _target.position.z + _playDistance.z),
            Time.deltaTime * _followSpeed);
        transform.rotation = Quaternion.Slerp(transform.rotation,
            Quaternion.Euler(_playAngle.x,
                            _playAngle.y,
                            _playAngle.z),
            Time.deltaTime * 1.5f);
    }

    internal void Reset()
    {
        this.Camera.fieldOfView = _startView;
        transform.position = _startPosition;
        transform.rotation = Quaternion.Euler(_startAngle);
    }
}