﻿Shader "add_TastyHomeShader/TH_Dissolve"
{
	Properties
	{
		[Enum(UnityEngine.Rendering.BlendMode)] _SrcBl ("Source Blend Mode", Float) = 5		    
		[Enum(UnityEngine.Rendering.BlendMode)] _DstBl ("Destination Blend Mode", Float) = 10   // One - additive, OneMinusSrcAlpha - Alpha Blending
        [Enum(UnityEngine.Rendering.CullMode)] _Culling ("Culling", Float) = 0
        
        [Space(16)]
        _ColorBase ("Color base",Color) = (1,1,1,1)

		[Space(10)]
		_AlphaEffect ("VertA A|Diss MaskUV A|Diss", Vector) = (0, 0, 0, 0)
		_FalloffUVParam ("MaskUV U01 V01", Vector) = (0, 0, 0, 0)

		[Space(16)]
        [NoScaleOffset] _Tex ("Texture BW", 2D) = "white" {}
		_Tex_TilingScroll  ("Tiling Scroll ", Vector) = (1,1,0,0)
		[Toggle] _CustomOffset ("Offset of custom data particle system", Float) = 0
		
		[Space(10)]
		[Header(Dissolve)]
		_Dissolve ("M0 M1 Smooth Lin", Vector) = (0.1, 0.1, 1, 1)

		[Space(10)]
		[Header(Edge)]
        _ColorEdge ("Color edge",Color) = (1,1,1,1)
		_Edge ("Width Smooth", Vector) = (0.1, 0.1, 1, 1)

	}

	SubShader
	{
		Tags
		{
			"RenderType" = "Transparent"
			"Queue" = "Transparent"
		}

		Blend [_SrcBl] [_DstBl]
		ZWrite Off
		Cull [_Culling]

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma shader_feature _CUSTOMOFFSET_ON

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;

				#if _CUSTOMOFFSET_ON
				float4 uv  : TEXCOORD0; //   particle custom data   zw - offset UV for random value
				float2 uv1 : TEXCOORD1; //   particle custom data   xy - offset UV for animated value
				#else
				float2 uv : TEXCOORD0;
				#endif

				fixed4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 uv  : TEXCOORD0;  // xy mainTex  zw original UV
				fixed4 color : COLOR;
			};

			fixed4 _ColorBase, _ColorEdge;
			float4 _AlphaEffect, _FalloffUVParam;

			sampler2D _Tex;
			float4 _Tex_TilingScroll;

			float4 _Dissolve;
			float2 _Edge;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex  = UnityObjectToClipPos(v.vertex);

				o.uv.zw = v.uv.xy;
				#if _CUSTOMOFFSET_ON   // custom offset of particle custom data
				v.uv.xy += float2(v.uv.z + v.uv1.x, v.uv.w + v.uv1.y);
				#endif
				o.uv.xy = v.uv.xy * _Tex_TilingScroll.xy + frac(_Tex_TilingScroll.zw * _Time.y);				

				o.color = v.color;
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col;

                // falloff UV
				float4 FalloffUV_vec = saturate((float4(i.uv.z, (1-i.uv.z), i.uv.w, (1-i.uv.w)) - 1.01)/(saturate(1-_FalloffUVParam) - 1.01));
				fixed FalloffUV = FalloffUV_vec.x * FalloffUV_vec.y * FalloffUV_vec.z * FalloffUV_vec.w;
				
				// maskUV and VertAlpha
				fixed2 mask = lerp(1, i.color.a, _AlphaEffect.xy) * lerp(1, FalloffUV, _AlphaEffect.zw);
							
				fixed tex_BW = tex2D(_Tex, i.uv.xy).r;
		
				// disslove
                fixed dissolve = lerp(_Dissolve.x, _Dissolve.y, pow(mask.y, _Dissolve.w));
                fixed2 maskDiss = saturate(((tex_BW + float2(dissolve, dissolve + _Edge.x)) * 50.0f - 25.0f) / float2(_Dissolve.z, _Edge.y));
                
                // color
                col.rgb = lerp( _ColorEdge.rgb, _ColorBase.rgb, maskDiss.x) * i.color.rgb;
                col.a = (maskDiss.x * _ColorBase.a + maskDiss.y * (1-maskDiss.x) * _ColorEdge.a) * mask.x;

				return col;
			}
			ENDCG
		}
	}
}
