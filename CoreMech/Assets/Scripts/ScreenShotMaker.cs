﻿using System.IO;
using UnityEngine;


namespace Maps.Debugin
{
    public class ScreenShotMaker : MonoBehaviour
    {
        public int num;

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                ScreenCapture.CaptureScreenshot(Path.Combine(Application.persistentDataPath, $"ScreenShot_{num}.png"));
                num++;
            }
        }
    }
}