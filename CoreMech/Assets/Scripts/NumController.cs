﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NumController : MonoBehaviour
{
    [SerializeField] private List<Transform> nums = new List<Transform>();

    public void SetNum(int n = 0)
    {
        int c = nums.Count;
        for (int i = 0; i < c; i++)
        {
            if (i != n)
                nums[i].gameObject.SetActive(false);
            else
                nums[i].gameObject.SetActive(true);
        }
    }

    [ContextMenu("AddNums")]
    private void AddNums()
    {
        nums = new List<Transform>();
        nums = GetComponentsInChildren<Transform>().ToList();
        nums.RemoveAt(0);
    }
}
