﻿using UnityEngine;

public class QuestionHolder : MonoBehaviour
{
    public static QuestionHolder Instance;
    [SerializeField] private Question _question;
    [SerializeField] private GlassPlatePairController _glassPair;

    public void Init(Question question, GlassPlatePairController pair)
    {
        _question = question;
        _glassPair = pair;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.Instance.DisplayQuestion(_question, _glassPair);
        }
    }

    private void Awake()
    {
        Instance = this;
    }

}
