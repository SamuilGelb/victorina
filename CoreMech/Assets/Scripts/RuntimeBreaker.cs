﻿using RayFire;
using UnityEngine;

public class RuntimeBreaker : MonoBehaviour
{
    public RayfireShatter shatter;
    [SerializeField] private bool _brokeOnAwake = false;

    public void Awake()
    {
        if (_brokeOnAwake)
        {
            shatter.Fragment();
            shatter.previewScale = 1.5f;
            GetComponent<MeshRenderer>().enabled = false;
            AddColliders();
            AddRigidbodies();
        }
    }

    private void AddRigidbodies()
    {
        foreach (var frag in shatter.fragmentsLast)
        {
            Rigidbody mc = frag.gameObject.GetComponent<Rigidbody>();
            if (mc == null)
                mc = frag.gameObject.AddComponent<Rigidbody>();
            mc.velocity = new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(0f, 2f), Random.Range(-0.1f, 0.1f));
        }
    }

    private void AddColliders()
    {
        foreach (var frag in shatter.fragmentsLast)
        {
            MeshCollider mc = frag.gameObject.GetComponent<MeshCollider>();
            if (mc == null)
                mc = frag.gameObject.AddComponent<MeshCollider>();
            mc.convex = true;
        }
    }
}
