﻿using UnityEngine;

public class DefeatController : MonoBehaviour
{
    public CameraController _cameraController;
    public LoseCanvasController _loseScreen;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Die();
        }
        else if (other.tag == "Bot")
        {
            other.transform.GetComponent<BotController>().Die();
        }
    }

    public void Die(bool lowCam = true)
    {
        GameManager.Instance.Player.Die();
        LeanTween.value(0, -10, 1f).setOnUpdate((float v) =>
        {
            if (lowCam)
                _cameraController._height = v;
        }).setOnComplete(_ =>
        {
            _loseScreen.Show();
        });
    }

    internal void Reset()
    {
        _loseScreen.Hide(true);
        _cameraController._height = 0;
    }
}
