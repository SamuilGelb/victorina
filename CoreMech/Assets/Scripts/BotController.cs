﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerController))]
public class BotController : MonoBehaviour
{
    private PlayerController controller;
    public PlayerController PlayerController
    {
        get
        {
            if (controller == null)
                controller = GetComponent<PlayerController>();
            return controller;
        }
    }
    [Header("Bot setup")]
    public Image _botFlag;
    public TextMeshProUGUI _botName;
    private bool _die;


    public void Die()
    {
        _die = true;
        PlayerController.Die();
    }

    public void InitBot(Sprite flag, string name, List<int> nums, Skin skin = null)
    {
        _botFlag.gameObject.SetActive(true);
        _botName.gameObject.SetActive(true);
        _botFlag.sprite = flag;
        _botName.text = name;
        if (skin != null)
            PlayerController.SetSkin(skin);
        PlayerController.SetNum(nums);
    }

    public void SetNewTarget(Transform left, Transform right, Side side)
    {
        if (_die) return;
        int r = Random.Range(0, 20);
        r += side == Side.Left ? -5 : 5;
        if (r >= 10)
        {
            PlayerController.ChangeTarget(right);
        }
        else
        {
            PlayerController.ChangeTarget(left);
        }
    }

    internal void SetSkin(Skin skin)
    {
        PlayerController.SetSkin(skin);
    }
}
