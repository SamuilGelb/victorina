﻿using UnityEngine;

public class BaseCanvasController : MonoBehaviour
{
    [SerializeField] protected RectTransform _mainPanel;
    [SerializeField] protected bool _displayOnAwake = false;
    [SerializeField] protected bool _isActive = false;

    public void Awake()
    {
        if (_displayOnAwake)
            Show();
        else
            Hide(true);
    }

    public virtual void Show()
    {
        if (_isActive) return;
        _mainPanel.gameObject.SetActive(true);
        _isActive = true;
    }

    public virtual void Hide(bool fast = false)
    {
        if (fast)
            _mainPanel.gameObject.SetActive(false);
        _isActive = false;
    }
}
