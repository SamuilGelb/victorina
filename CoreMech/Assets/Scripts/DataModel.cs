﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[Serializable]
public class DataModel
{
    public List<Question> Questions;
    public Reward Reward;
    public int Num;

    public DataModel()
    {
        Questions = new List<Question>();
        Reward = new Reward();
        Num = 0;
    }
}

[Serializable]
public class Question
{
    public DataUnit question;
    public Answer RightAnswer;
    public Answer WrongAnswer;
    public Reward QuestionReward;
    public bool Rewarded;

    public Question()
    {
        question = new DataUnit();
        RightAnswer = new Answer();
        WrongAnswer = new Answer();
        QuestionReward = new Reward();
        Rewarded = false;
    }
}

[Serializable]
public class Answer
{
    public DataUnit answer;
    public bool IsCorrect;

    public Answer()
    {
        answer = new DataUnit();
        IsCorrect = false;
    }
}

[Serializable]
public class DataUnit
{
    public string Text;
    public string ImageURL;
    public string ImageName;
}

[Serializable]
public class Reward
{
    public int Money;
    public string Skin;
    public int Hint;
    public int Rating;
}

public static class DataModelUtils
{
    public static ImageSource ContainImage(DataUnit data)
    {
        if (data.ImageURL.Contains("http"))
            return ImageSource.Link;
        else if (!string.IsNullOrEmpty(data.ImageName))
            return ImageSource.Resources;
        else
            return ImageSource.Null;
    }

    public static IEnumerator DownloadImage(string url, Image toImage)
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
        {
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
                toImage.sprite = null;
            }
            else
            {
                var texture = DownloadHandlerTexture.GetContent(uwr);
                Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                toImage.sprite = sprite;
            }
        }
    }

    public static Sprite GetImage(string fileName)
    {
        Sprite image = Resources.Load<Sprite>(fileName);
        if (image != null)
            return image;
        return null;
    }
}

public enum ImageSource
{
    Link,
    Resources,
    Null
}