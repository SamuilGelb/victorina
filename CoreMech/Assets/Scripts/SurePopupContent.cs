﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SurePopupContent : BasePopupController
{
    public static SurePopupContent Instance;
    [SerializeField] private Button _yes;
    [SerializeField] private Button _no;

    private Action<bool> _popupAction;

    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        _yes.onClick.AddListener(() => DoAction(true));
        _no.onClick.AddListener(() => DoAction(false));
    }

    private void DoAction(bool value)
    {
        if (_popupAction != null)
            _popupAction(value);
        _tweenPopupController.Hide(false);
    }

    /// <summary>
    /// For SurePopup send Action <<bool>>
    /// </summary>
    /// <param name="args"></param>
    public override void Init(params object[] args)
    {
        _popupAction = args[0] as Action<bool>;
        base.Init(args);
    }
}
