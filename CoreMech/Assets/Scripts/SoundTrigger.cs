﻿using System.Collections.Generic;
using UnityEngine;

public class SoundTrigger : MonoBehaviour
{
    public string Tag = "FallHit";
    public AudioSource _source;
    public List<AudioClip> _allowedClips = new List<AudioClip>();

    public bool playOnAwake = false;
    public bool playOneTime = true;
    private bool played;

    public void Awake()
    {
        if (playOnAwake)
            PlayRandom();
    }

    public void PlayRandom()
    {
        if ((playOneTime && played) || !GameManager.Instance.UserData.Sound) return;
        played = true;
        _source.PlayOneShot(_allowedClips[Random.Range(0, _allowedClips.Count)]);
    }
}
