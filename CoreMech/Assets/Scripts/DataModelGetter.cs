﻿using System;
using System.Collections;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class DataModelGetter
{
    private const string pass1 = "pR59O]l4#LV|[wH}f:jq&h6Sg{svIA";
    private const string pass2 = "L18uR@Eh]qpbPi[H0-_4J,K:";
    private const string metod = "MD5";
    private const int pass_interation = 16;
    private const string initial_vector = "Qsd23Mkjk7898*hj";
    private const int keySize = 256;

    /// <summary>
    /// Get level data from sources. 
    /// If path contains .ldm - file, if contains http: - link, if is numeric - request, else - resources
    /// </summary>
    /// <param name="path">path to level data</param>
    /// <returns></returns>
    public async static Task<DataModel> GetData(string path)
    {
        int num = 0;
        bool result = int.TryParse(path, out num);
        if (result)
        {
            return await GetRequestData(num);
        }
        else if (path.Contains("http"))
        {
            return await GetLinkData(path);
        }
        else if (path.Contains(".ldm"))
        {
            return await GetFileData(path);
        }
        else
        {
            return await GetResourcesData(path);
        }
    }

    private async static Task<DataModel> GetFileData(string filename)
    {
        var data = File.ReadAllText(Path.Combine(Application.persistentDataPath, "Files", filename));
        await Task.Delay(90);
        string FinalValue = EncryptUtil.Decrypt(data, pass1, pass2, metod, pass_interation, initial_vector, keySize);
        try
        {
            DataModel result = JsonUtility.FromJson<DataModel>(FinalValue);
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        return null;
    }

    private async static Task<DataModel> GetRequestData(int levelNum)
    {
        await Task.Delay(200);
        return null;
    }

    private async static Task<DataModel> GetResourcesData(string filename)
    {
        string data = Resources.Load<TextAsset>($"{filename}").text;
        await Task.Delay(100);
        string FinalValue = EncryptUtil.Decrypt(data, pass1, pass2, metod, pass_interation, initial_vector, keySize);
        try
        {
            DataModel result = JsonUtility.FromJson<DataModel>(FinalValue);
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        return null;
    }

    private async static Task<DataModel> GetLinkData(string link)
    {
        string data = "";
        await Task.Delay(300);
        string FinalValue = EncryptUtil.Decrypt(data, pass1, pass2, metod, pass_interation, initial_vector, keySize);
        try
        {
            DataModel result = JsonUtility.FromJson<DataModel>(FinalValue);
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        return null;
    }

    internal async static Task<UserData> GetUserData()
    {
        UserData data = new UserData();
        string path = Path.Combine(Application.persistentDataPath, "Userdata.udm");
        string str = "";
        if (File.Exists(path))
        {
            str = File.ReadAllText(path);
            var decode = EncryptUtil.Decrypt(str, pass1, pass2, metod, pass_interation, initial_vector, keySize);
            data = JsonUtility.FromJson<UserData>(decode);
        }
        else
        {
            str = JsonUtility.ToJson(data);
            var encode = EncryptUtil.Encrypt(str, pass1, pass2, metod, pass_interation, initial_vector, keySize);
            StreamWriter sw = File.CreateText(path);
            sw.WriteLine(encode);
            sw.Close();
        }
        await Task.Delay(400);
        return data;
    }

    public async static void SaveUserData(UserData data)
    {
        string path = Path.Combine(Application.persistentDataPath, "Userdata.udm");
        string str = JsonUtility.ToJson(data);
        string encode = EncryptUtil.Encrypt(str, pass1, pass2, metod, pass_interation, initial_vector, keySize);
        StreamWriter sw = File.CreateText(path);
        sw.WriteLine(encode);
        sw.Close();
        await Task.Delay(200);
    }

    public static void SaveDataModel(DataModel model, string filename, ModelSource target = ModelSource.DataPath)
    {
        string data = JsonUtility.ToJson(model);
        string FinalValue = EncryptUtil.Encrypt(data, pass1, pass2, metod, pass_interation, initial_vector, keySize);
        string filepath = "";
        switch (target)
        {
            case ModelSource.Resources:
                filepath = $"{Path.Combine(Application.dataPath, "Resources", filename)}.txt";
                break;
            case ModelSource.DataPath:
                filepath = $"{Path.Combine(Application.persistentDataPath, "Files", filename)}.ldm";
                break;
            case ModelSource.Server:
                break;
            case ModelSource.Request:
                break;
            default:
                break;
        }
        if (!File.Exists(filepath))
        {
            File.Create(filepath);
            Thread.Sleep(200);
        }
        try
        {
            File.WriteAllText(filepath, FinalValue);
            _catched = false;
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            if (!_catched)
            {
                SaveDataModel(model, filename, target);
                _catched = true;
            }
            else
            {
                throw ex;
            }
        }

    }

    private static bool _catched = false;
}

public enum ModelSource
{
    Resources,
    DataPath,
    Server,
    Request
}