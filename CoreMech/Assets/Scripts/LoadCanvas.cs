﻿using UnityEngine;
using UnityEngine.UI;

public class LoadCanvas : BaseCanvasController
{
    [SerializeField] private Slider _loadBar;
    public static LoadCanvas Instance;

    public new void Awake()
    {
        Instance = this;
        base.Awake();
    }

    public void InitMaxValue(int x)
    {
        _loadBar.maxValue = x;
    }

    public void ChangeValue(int added)
    {
        _loadBar.value += added;
        if (_loadBar.value >= _loadBar.maxValue)
        {
            base.Hide(true);
        }
    }
}