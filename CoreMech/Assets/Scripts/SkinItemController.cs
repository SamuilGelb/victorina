﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SkinItemController : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] private Button _skinButton;
    public Button SkinButton
    {
        get
        {
            if (_skinButton == null)
                _skinButton = GetComponent<Button>();
            return _skinButton;
        }
    }
    [SerializeField] private Image _skinImage;
    [SerializeField] private TextMeshProUGUI _cost;
    [SerializeField] private RectTransform _costBox;
    [SerializeField] private RectTransform _checkMark;
    [Header("Internal")]
    [SerializeField] internal bool _available;
    [SerializeField] internal Skin _skinValue;
    [SerializeField] internal SkinSelectCanvasController Controller;

    public void Init(Skin value, bool available, bool active = false)
    {
        _skinValue = value;
        _available = available;
        _skinImage.sprite = value._skinView;
        _cost.text = $"{value._skinCost}";
        _costBox.gameObject.SetActive(!available);
        _checkMark.gameObject.SetActive(active);
    }

    public void Activate(bool activate)
    {
        _checkMark.gameObject.SetActive(activate);
    }

    public void Awake()
    {
        SkinButton.onClick.AddListener(() => OnClicked());
    }

    internal void Unlock()
    {
        _costBox.gameObject.SetActive(false);
        _available = true;
    }

    private void OnClicked()
    {
        if (!_available)
        {
            Action<bool> action = OnBuyed;
            BuyPopupController.Instance.Init(_skinValue, action);
        }
        else
        {
            GameManager.Instance.ActivateSkin(_skinValue);
            Controller.ActivateSkin(_skinValue);
        }
    }

    public void OnBuyed(bool success)
    {
        if(success)
        {
            Controller.UnlockSkin(_skinValue);
            GameManager.Instance.ApplyReward(
                new Reward() { 
                    Money = -_skinValue._skinCost, 
                    Skin = _skinValue._name 
                });
            //GameManager.Instance.AddSkin(_skinValue._name);
        }
        else
        {
            Debug.LogError("Error, while trying to buy skin");
        }
    }
}
