﻿using UnityEngine;

public class BasePopupController : MonoBehaviour
{
    public BaseTweenPopupController _tweenPopupController;

    public virtual void Init(params object[] args)
    {
        _tweenPopupController.Show();
    }
}