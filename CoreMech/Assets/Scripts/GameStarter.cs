﻿using System.Collections;
using UnityEngine;

public class GameStarter : MonoBehaviour
{
    public PlayerController _playerController;
    public BotsController _botsController;
    public CameraController _cameraController;
    public MainCanvasControll _mainCanvas;

    private IEnumerator StartGameIn5s()
    {
        yield return new WaitForSeconds(5f);
        StartGame();
    }

    public void StartGame()
    {
        _playerController.ResetPosition();
        _botsController.UpdateGraphics();
        _botsController.SetDefaultTarget();
        _cameraController._follow = true;
        _mainCanvas.Show();
    }

    public void PauseGame()
    {
        _mainCanvas.Hide(true);
        _playerController.ResetPosition();
        _playerController.ChangeTarget(null);
        _botsController.ClearBots();
        _botsController.SpawnBots(true);
        _cameraController._follow = false;
        _cameraController.Reset();
    }
}
