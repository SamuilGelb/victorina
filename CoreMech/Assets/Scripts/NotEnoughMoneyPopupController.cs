﻿using TMPro;
using System;
using UnityEngine;
using UnityEngine.UI;

public class NotEnoughMoneyPopupController : BasePopupController
{
    [Header("References")]
    [SerializeField] private TextMeshProUGUI _title;
    [SerializeField] private Button _cancelButton;
    [SerializeField] private Button _applyButton;
    [SerializeField] private Button _watchAdButton;
    [Header("Internal")]
    [SerializeField] Action Canceled;

    public static NotEnoughMoneyPopupController Instance;

    public void Awake()
    {
        Instance = this;
        _cancelButton.onClick.AddListener(() => OnCanvel());
        _applyButton.onClick.AddListener(() => OnPurchase());
        _watchAdButton.onClick.AddListener(() => WatchAds());
    }

    private void WatchAds()
    {
        UnityAds.Instance.LoadRewardedVideo();
        UnityAds.Instance.Rewarded += AdsRewarded;
    }

    private void AdsRewarded()
    {
        GameManager.Instance.ApplyReward(new Reward() { Money = 100 });
        _tweenPopupController.Hide(false);
        UnityAds.Instance.Rewarded -= AdsRewarded;
    }

    private void OnPurchase()
    {
        //OpenPurchaseShop
    }

    private void OnCanvel()
    {
        _tweenPopupController.Hide(false);
    }
}
