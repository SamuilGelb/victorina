﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "NewStringPool", menuName = "Scriptable Objects/String Pool")]
public class StringPool : ScriptableObject
{
    public List<string> _strings = new List<string>();
    public TextAsset _source;

    [ContextMenu("Get strings")]
    public void Parce()
    {
        _strings = new List<string>();
        _strings = _source.text.Split(',').ToList();
        Debug.Log("Parsed");
    }
}
