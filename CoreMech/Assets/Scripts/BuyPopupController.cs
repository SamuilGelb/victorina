﻿using TMPro;
using System;
using UnityEngine;
using UnityEngine.UI;

public class BuyPopupController : BasePopupController
{
    [Header("References")]
    [SerializeField] private TextMeshProUGUI _title;
    [SerializeField] private SkinItemController _skinItemController;
    [SerializeField] private Button _cancelButton;
    [SerializeField] private Button _buyButton;
    [Header("Internal")]
    [SerializeField] private Skin _skin;
    private Action<bool> OnBuy; 
    public static BuyPopupController Instance;

    public void Awake()
    {
        Instance = this;
        _cancelButton.onClick.AddListener(() => CanvelBuy());
        _buyButton.onClick.AddListener(() => BuyItem());
    }

    private void BuyItem()
    {
        if (GameManager.Instance.UserData.Coins >= _skin._skinCost)
        {
            if (OnBuy != null)
            {
                OnBuy(true);
                _tweenPopupController.Hide(false);
                SuccessPopupController.Instance.Init(_skin);
            }
            else
                Debug.LogError("OnBuy is empty");
        }
        else
        {
            NotEnoughMoneyPopupController.Instance.Init();
        }
    }

    private void CanvelBuy()
    {
        Debug.Log("Canceled buy");
        _tweenPopupController.Hide(false);
        //if(OnBuy != null)
        //{
        //    OnBuy(false);
        //}
    }

    /// <summary>
    /// For BuyPopup send Skin, Action<<bool>>
    /// </summary>
    /// <param name="args"></param>
    public override void Init(params object[] args)
    {
        _skin = args[0] as Skin;
        OnBuy = args[1] as Action<bool>;
        _skinItemController.Init(_skin, false, false);
        base.Init(null);
    }
}
