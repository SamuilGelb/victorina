﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkinSelectCanvasController : BaseCanvasController
{
    public TextMeshProUGUI _coinCount;
    public Button _backButton;
    public SkinsPool _skinPool;
    public SkinItemController _skinItemPrefab;
    public RectTransform _content;
    private List<SkinItemController> _skinItems = new List<SkinItemController>();

    public BasePopupController BuyPopup;
    public TweenPopupMoveToCoords NotEnough;
    public TweenPopupMoveToCoords Success;

    public new void Awake()
    {
        base.Awake();
        GameManager.OnUserDataChanged += UpdateCoins;
        _backButton.onClick.AddListener(() => { Hide(true); });
    }

    private void UpdateCoins()
    {
        _coinCount.text = $"{GameManager.Instance.UserData.Coins}";
    }

    public void Init()
    {
        ClearSkins();
        int c = _skinPool._skinList.Count;
        for (int i = 0; i < c; i++)
        {
            var skinItem = Instantiate(_skinItemPrefab, _content);
            skinItem.Init(_skinPool._skinList[i],
                PlayerContain(_skinPool._skinList[i]),
                IsActiveSkin(_skinPool._skinList[i]));
            skinItem.Controller = this;
            _skinItems.Add(skinItem);
        }
        Show();
    }

    private void ClearSkins()
    {
        int c = _skinItems.Count;
        for (int i = 0; i < c; i++)
        {
            Destroy(_skinItems[i].gameObject);
        }
        _skinItems.Clear();
    }

    private bool IsActiveSkin(Skin skin)
    {
        return GameManager.Instance.UserData.ActiveSkin == skin._name;
    }

    private bool PlayerContain(Skin skin)
    {
        return GameManager.Instance.UserData.Skins.ToList().Contains(skin._name);
    }

    internal void ActivateSkin(Skin skinValue)
    {
        int c = _skinItems.Count;
        for (int i = 0; i < c; i++)
        {
            _skinItems[i].Activate(_skinItems[i]._skinValue._name == skinValue._name);
        }
    }

    internal void UnlockSkin(Skin skinValue)
    {
        int c = _skinItems.Count;
        for (int i = 0; i < c; i++)
        {
            if (_skinItems[i]._skinValue._name == skinValue._name)
                _skinItems[i].Unlock();
        }
    }
}
