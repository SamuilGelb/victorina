﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "NewSpritePool", menuName = "Scriptable Objects/Sprite Pool")]
public class SpritePool : ScriptableObject
{
    public List<SpriteItem> _spritePool = new List<SpriteItem>();
    public Texture2D _source;

#if UNITY_EDITOR
    [ContextMenu("Get sprites")]
    public void GetSprites()
    {
        _spritePool = new List<SpriteItem>();
        string spriteSheet = AssetDatabase.GetAssetPath(_source);
        List<Sprite> sprites = AssetDatabase.LoadAllAssetsAtPath(spriteSheet).OfType<Sprite>().ToList();
        int c = sprites.Count;
        for (int i = 0; i < c; i++)
        {
            _spritePool.Add(new SpriteItem() { sprite = sprites[i], name = _source.name, number = i });
        }
        Debug.Log("Parsed");
    }
#endif
}

[System.Serializable]
public class SpriteItem
{
    public Sprite sprite;
    public string name;
    public int number;
}
