﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RagdollController : MonoBehaviour
{
    public List<Rigidbody> _rigidbodies = new List<Rigidbody>();
    public List<Collider> _colliders = new List<Collider>();

    public void Awake()
    {
        FillLists();
        DeactivateRagdoll();
    }

    private void FillLists()
    {
        _rigidbodies = GetComponentsInChildren<Rigidbody>().ToList();
        _colliders = GetComponentsInChildren<Collider>().ToList();
    }

    public void DeactivateRagdoll()
    {
        _rigidbodies[0].WakeUp();
        _rigidbodies[0].constraints = (RigidbodyConstraints)80;
        _colliders[0].enabled = true;
        int c = _rigidbodies.Count;
        for (int i = 1; i < c; i++)
        {
            _rigidbodies[i].isKinematic = true;
            _rigidbodies[i].Sleep();
        }
        c = _colliders.Count;
        for (int i = 1; i < c; i++)
        {
            _colliders[i].enabled = false;
        }
    }

    public void ActivateRagdoll()
    {
        GetComponentsInChildren<SoundTrigger>().ToList().First(x => x.Tag == "Scream").PlayRandom();
        _rigidbodies[0].Sleep();
        _rigidbodies[0].constraints = RigidbodyConstraints.FreezeAll;
        _colliders[0].enabled = false;
        int c = _rigidbodies.Count;
        for (int i = 1; i < c; i++)
        {
            _rigidbodies[i].isKinematic = false;
            _rigidbodies[i].WakeUp();
        }
        c = _colliders.Count;
        for (int i = 1; i < c; i++)
        {
            _colliders[i].enabled = true;
        }
    }


}
