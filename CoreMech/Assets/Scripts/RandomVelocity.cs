﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RandomVelocity : MonoBehaviour
{
    public Rigidbody _rigidbody;

    public Rigidbody Rigidbody
    {
        get
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody>();
            return _rigidbody;
        }
    }

    private void Awake()
    {
        Rigidbody.velocity = new Vector3(Random.Range(-0.5f, 0.5f), 0, Random.Range(-1.5f, 1.5f));
    }

    [ContextMenu("Set Rigidbody")]
    public void SetRigidbody()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
}
