﻿using System.Linq;
using UnityEngine;

public class BrokenPlateController : ResultPlateController
{
    private void Awake()
    {
        DoBroken();
    }

    public override void DoBroken()
    {
        base.DoBroken();
        ActivateRigidboides();
    }

    private void ActivateRigidboides()
    {
        var childs = GetComponentsInChildren<Rigidbody>().ToList();
        int c = childs.Count;
        for (int i = 0; i < c; i++)
        {
            float force = Random.Range(0, -5f);
            childs[i].velocity = new Vector3(0, force, 0);
        }
    }
}
