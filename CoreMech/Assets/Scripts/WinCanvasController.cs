﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WinCanvasController : BaseCanvasController
{
    [Header("Linked")]
    [SerializeField] private EarnMoneyUIController _earnController;
    [SerializeField] private Image _panel;
    [SerializeField] private RectTransform _popup;
    [SerializeField] private TextMeshProUGUI _subTitle;
    [SerializeField] private TextMeshProUGUI _coinsReward;
    [SerializeField] private TextMeshProUGUI _scoreReward;
    [SerializeField] private TextMeshProUGUI _reputationReward;
    [SerializeField] private Button _multiplyReward;
    [SerializeField] private Button _continueButton;
    [SerializeField] private Button _quitButton;

    private DataModel _levelData;

    private new void Awake()
    {
        base.Awake();
        _multiplyReward.gameObject.SetActive(true);
        _multiplyReward.onClick.AddListener(() => { UnityAds.Instance.LoadRewardedVideo(); UnityAds.Instance.Rewarded += Ads_Rewarded; });
        _quitButton.onClick.AddListener(() => { GameManager.Instance.BackToMenu(); Hide(false); });
        _continueButton.onClick.AddListener(() => { GameManager.Instance.LoadLevel(LevelToLoad.Next); Hide(false); });
    }

    private void Ads_Rewarded()
    {
        if (_levelData != null)
        {
            _levelData.Reward.Money *= 2;
            GameManager.Instance.ApplyReward();
            _coinsReward.text = $"Coints: x{_levelData.Reward.Money}";
        }
        else
        {
            _levelData = new DataModel() { Reward = new Reward() { Money = 100 } };
            Ads_Rewarded();
        }
        _multiplyReward.gameObject.SetActive(false);
        UnityAds.Instance._wached = true;
        UnityAds.Instance.Rewarded -= Ads_Rewarded;
    }

    private void OnDisable()
    {
        _quitButton.onClick.RemoveAllListeners();
        _continueButton.onClick.RemoveAllListeners();
    }

    public void Init()
    {
        _levelData = GameManager.Instance.CurrentLevelData;
        _multiplyReward.gameObject.SetActive(true);
        _subTitle.text = $"You survived {_levelData.Num} day";
        _coinsReward.text = $"Coints: x0";
        _scoreReward.text = $"Score: x0";
        _reputationReward.text = $"Reputation: x0";
        Show();
    }

    public override void Show()
    {
        base.Show();
        _earnController.gameObject.SetActive(false);
        _popup.anchoredPosition = new Vector2(0, -Screen.height);
        LeanTween.color(_panel.rectTransform, new Color(0, 0, 0, 0.5f), 0.5f).setOnUpdate(
            (Color col) =>
            {
                _panel.color = col;
            }).setOnComplete(_ =>
            {
                _earnController.gameObject.SetActive(true);
            });
        LeanTween.value(-Screen.height, -150, 1f).setDelay(0.5f).setOnUpdate(
            (float v) =>
            {
                _popup.anchoredPosition = new Vector2(0, v);
            });
        LeanTween.value(-150, -250, 0.2f).setDelay(1.5f).setOnUpdate(
            (float v) =>
            {
                _popup.anchoredPosition = new Vector2(0, v);
            }).setOnComplete(_ =>
            {
                _quitButton.gameObject.SetActive(true);
                AnimValues();
            });
    }

    private void AnimValues()
    {
        LeanTween.value(0, _levelData.Reward.Money, 0.7f).setOnUpdate((float v) =>
        {
            _coinsReward.text = $"Coints: x{(int)v}";
        });
        LeanTween.value(0, 1000, 0.7f).setDelay(0.7f).setOnUpdate((float v) =>
        {
            _scoreReward.text = $"Score: x{(int)v}";
        });
        LeanTween.value(0, _levelData.Reward.Rating, 0.7f).setDelay(1.4f)
            .setOnUpdate((float v) =>
            {
                _reputationReward.text = $"Reputation: x{(int)v}";
            });
    }

    public override void Hide(bool fast = false)
    {
        base.Hide(fast);
        LeanTween.value(-250, -150, 0.2f).setOnUpdate(
            (float v) =>
            {
                _popup.anchoredPosition = new Vector2(0, v);
            }).setOnComplete(_ => _quitButton.gameObject.SetActive(false));
        LeanTween.value(-150, -Screen.height, 1f).setDelay(0.2f).setOnUpdate(
            (float v) =>
            {
                _popup.anchoredPosition = new Vector2(0, v);
            });
        LeanTween.color(_panel.rectTransform, new Color(0, 0, 0, 0.0f), 0.5f).setDelay(1.2f).setOnUpdate(
            (Color col) =>
            {
                _panel.color = col;
            }).setOnComplete(_ =>
            {
                _earnController.gameObject.SetActive(false);
            });
    }
}
