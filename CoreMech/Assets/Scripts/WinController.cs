﻿using UnityEngine;

public class WinController : MonoBehaviour
{
    public Transform _finish;
    public WinCanvasController _winCanvas;
    public GameObject _winObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _winObject.SetActive(true);
            _winCanvas.Init();
            GameManager.Instance.ApplyReward();
        }
    }

    public void Awake()
    {
        Reset();
    }

    internal void Reset()
    {
        _winObject.SetActive(false);
        _winCanvas.Hide(true);
    }
}
