﻿using System.Collections.Generic;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
    [Header("CreatedObjects")]
    private List<GameObject> _createdObjects = new List<GameObject>();
    public static List<GameObject> _generatedObjects = new List<GameObject>();
    [Header("Prefabs")]
    [SerializeField] private GlassPlatePairController _glassPlate;
    [SerializeField] private Transform _rope;
    [SerializeField] private Transform _startBlock;
    [SerializeField] private Transform _endBlock;
    [Header("Const Data")]
    public const float PLATE_SIZE = 2.7f;
    public const float HOLE_SIZE = 1.2f;
    public const float DEFAULT_HEIGHT = 5;
    [Header("Internal values")]
    private Vector3 p_lastPoint = new Vector3(0f, 5f, 10f);
    [Header("DebugMode")]
    [SerializeField] private DataModel _model;

    public Vector3 GetLevelSize()
    {
        return new Vector3(p_lastPoint.z, 20f, p_lastPoint.z);
    }

    public Vector3 GenerateLevel(DataModel levelData)
    {
        _model = levelData;
        _startBlock.position = Vector3.zero;
        int questCount = levelData.Questions.Count;
        //Generation

        //Rope
        p_lastPoint = new Vector3(0, 5, 10f);
        p_lastPoint = GetNextCordinates(GenerateObjectType.Rope, 0);
        var rotation = Quaternion.Euler(90, 0, 0);
        var scale = new Vector3(0.1f, p_lastPoint.z - 10f, 0.1f);
        var ropeR_L = Instantiate(_rope, Vector3.zero, rotation);
        ropeR_L.position = p_lastPoint + new Vector3(0.2f, 0, 0);
        ropeR_L.localScale = scale;
        var ropeL_R = Instantiate(_rope, Vector3.zero, rotation);
        ropeL_R.position = p_lastPoint + new Vector3(-0.2f, 0, 0);
        ropeL_R.localScale = scale;
        var ropeL_L = Instantiate(_rope, Vector3.zero, rotation);
        ropeL_L.position = p_lastPoint + new Vector3(-2.95f, 0, 0);
        ropeL_L.localScale = scale;
        var ropeR_R = Instantiate(_rope, Vector3.zero, rotation);
        ropeR_R.position = p_lastPoint + new Vector3(2.95f, 0, 0);
        ropeR_R.localScale = scale;
        _createdObjects.Add(ropeL_L.gameObject);
        _createdObjects.Add(ropeL_R.gameObject);
        _createdObjects.Add(ropeR_L.gameObject);
        _createdObjects.Add(ropeR_R.gameObject);

        //Glass
        p_lastPoint = new Vector3(0f, 5f, 10f);
        GlassPlatePairController prevPair = null;
        for (int i = 0; i < questCount; i++)
        {
            var glassPlatePair = Instantiate(_glassPlate, Vector3.zero, Quaternion.identity);
            p_lastPoint = GetNextCordinates(GenerateObjectType.GlassPlate, i);
            glassPlatePair.transform.position = p_lastPoint;
            _createdObjects.Add(glassPlatePair.gameObject);
            if (i == 0)
            {
                QuestionHolder.Instance.Init(_model.Questions[i], glassPlatePair);
                prevPair = glassPlatePair;
            }
            else
            {
                prevPair.Init(_model.Questions[i - 1], _model.Questions[i], glassPlatePair);
            }
            prevPair = glassPlatePair;
            if (i + 1 == questCount)
            {
                glassPlatePair.Init(_model.Questions[i], null, null);
            }
        }
        p_lastPoint += new Vector3(0, -5, 12.25f);
        _endBlock.transform.position = p_lastPoint;
        Vector3 size = new Vector3(p_lastPoint.z, 20f, p_lastPoint.z);

        return size;
    }

    internal void ClearLevel()
    {
        int c = _createdObjects.Count;
        for (int i = 0; i < c; i++)
        {
            Destroy(_createdObjects[i]);
        }
        _createdObjects.Clear();
        c = _generatedObjects.Count;
        for (int i = 0; i < c; i++)
        {
            Destroy(_generatedObjects[i]);
        }
        _generatedObjects.Clear();
    }

    private Vector3 GetNextCordinates(GenerateObjectType pathBlock, int i)
    {
        Vector3 nextCord = Vector3.zero;
        switch (pathBlock)
        {
            case GenerateObjectType.GlassPlate:
                if (i == 0)
                    nextCord = p_lastPoint + new Vector3(0, 0, PLATE_SIZE / 2 + HOLE_SIZE);
                else
                    nextCord = p_lastPoint + new Vector3(0, 0, PLATE_SIZE + HOLE_SIZE);
                break;
            case GenerateObjectType.Rope:
                nextCord = p_lastPoint + new Vector3(0, 0, HOLE_SIZE / 2 - 0.15f) + new Vector3(0, 0, PLATE_SIZE + HOLE_SIZE) * (_model.Questions.Count / 2f);
                break;
            default:
                nextCord = new Vector3(0f, 5f, 10f);
                break;
        }
        return nextCord;
    }

    internal static void AddGeneratedObject(GameObject objects)
    {
        _generatedObjects.Add(objects);
    }

    internal static bool RemoveGeneratedObject(GameObject objects)
    {
        if (_generatedObjects.Contains(objects))
        {
            _generatedObjects.Remove(objects);
            return true;
        }
        return false;
    }
}

internal enum GenerateObjectType
{
    GlassPlate,
    Rope,
}
