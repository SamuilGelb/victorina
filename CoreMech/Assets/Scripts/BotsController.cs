﻿using System.Collections.Generic;
using UnityEngine;

public class BotsController : MonoBehaviour
{
    [Range(0, 16)]
    [SerializeField] private int _botsCount;
    [SerializeField] private BotController _botPrefab;
    [SerializeField] private Rect _botSpawnZone;
    [SerializeField] private Transform _defaultTarget;
    [Header("Hiden")]
    private List<BotController> _botsList = new List<BotController>();
    [SerializeField] private SpritePool _flagsPool;
    [SerializeField] private StringPool _namePool;
    [SerializeField] private SkinsPool _skins;
    private List<string> _uNums = new List<string>();

    private void Awake()
    {
        //SpawnBots();
    }

    public void ClearBots()
    {
        int c = _botsList.Count;
        for (int i = 0; i < c; i++)
        {
            Destroy(_botsList[i].gameObject);
        }
        _botsList.Clear();
        _uNums.Clear();
    }

    public void SpawnBots(bool init = false)
    {
        ClearBots();
        List<(int, int)> ps = new List<(int, int)>();
        for (int i = 0; i < _botsCount; i++)
        {
            Vector2 coord = GetCords(ps);
            var bot = Instantiate(_botPrefab, new Vector3(coord.x, 5, coord.y), Quaternion.identity);
            _botsList.Add(bot);
            _botsList[i].PlayerController.ChangeOffset(.4f);
            if (!init)
            {
                bot.InitBot(GetRandomFlag(), GetRandomName(), GetNumber(), RandomSkin());
                SetDefaultTarget();
            }
            else
                bot.SetSkin(RandomSkin());
        }
    }

    private Skin RandomSkin()
    {
        return _skins._skinList[Random.Range(0, _skins._skinList.Count)];
    }

    internal void UpdateGraphics()
    {
        int c = _botsList.Count;
        for (int i = 0; i < c; i++)
        {
            _botsList[i].InitBot(GetRandomFlag(), GetRandomName(), GetNumber());
        }
    }

    internal void SetDefaultTarget()
    {
        int c = _botsList.Count;
        for (int i = 0; i < c; i++)
        {
            _botsList[i].SetNewTarget(_defaultTarget, _defaultTarget, Side.Left);
        }
    }

    private List<int> GetNumber()
    {
        int f, s, t;
        f = Random.Range(0, 10);
        s = Random.Range(0, 10);
        t = Random.Range(0, 10);
        if (_uNums.Contains($"{f}{s}{t}") || $"{f}{s}{t}" == "001")
            return GetNumber();
        else
            _uNums.Add($"{f}{s}{t}");
        return new List<int>() { f, s, t };
    }

    private string GetRandomName()
    {
        int x = Random.Range(0, _namePool._strings.Count);
        return _namePool._strings[x];
    }

    private Sprite GetRandomFlag()
    {
        int x = Random.Range(0, _flagsPool._spritePool.Count);
        return _flagsPool._spritePool[x].sprite;
    }

    private Vector2 GetCords(List<(int, int)> ps)
    {
        float x = _botSpawnZone.x + Random.Range(0, _botSpawnZone.width);
        float y = _botSpawnZone.y + Random.Range(0, _botSpawnZone.height);
        int _x = Mathf.RoundToInt(x);
        int _y = Mathf.RoundToInt(y);
        if (ps.Contains((_x, _y)) || (_x == 0 && y == 0))
            return GetCords(ps);
        else
            ps.Add((_x, _y));
        Vector2 result = new Vector2(x, y);
        return result;
    }

    internal void ChooseAction(GlassPlatePairController nextGlassPair, Side side)
    {
        int c = _botsList.Count;
        for (int i = 0; i < c; i++)
        {
            _botsList[i].PlayerController.ChangeOffset(0.1f);
            _botsList[i].SetNewTarget(nextGlassPair.GetNextPoint(Side.Left), nextGlassPair.GetNextPoint(Side.Right), side);
        }
    }
}
