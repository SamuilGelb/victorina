﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;

public static class EncryptUtil
{
    /************************************** КРИПТОГРАФИЯ АЛГОРИТМ AES *******************************/
    /*

     * HashAlgorithm может быть SHA1 или MD5.

     * InitialVector должен быть строкой размерностью 16 ASCII символов.

     * KeySize может быть 128, 192, или 256.

     * The Salt выступает в роли второго ключа.

     * PasswordIterations сколько раз алгоритм выполнится над текстом.

     Использовать функции довольно просто. (Не забываем подключать необходимые сборки System.Security.Cryptography)


             using System.Security.Cryptography;

             зашифровать:
             string FinalValue=Encrypt("Text", "Password1", "Password2", "SHA1", 2, "16CHARSLONG12345", 256);  

             дешифровать:
             string FinalValue=Decrypt("Text", "Password1", "Password2", "SHA1", 2, "16CHARSLONG12345", 256);   ******/



    /****************** ШИФРОВАНИЕ **************************/
    /// <summary> /// Encrypts a string /// </summary>
    ///Text to be encrypted
    ///Password to encrypt with
    ///Salt to encrypt with
    ///Can be either SHA1 or MD5
    ///Number of iterations to do
    ///Needs to be 16 ASCII characters long
    ///Can be 128, 192, or 256
    /// <returns>An encrypted string</returns>
    public static string Encrypt(string plainText, string password, string salt, string hashAlgorithm, int passwordIterations, string initialVector, int keySize)
    {
        if (string.IsNullOrEmpty(plainText))
            return "";
        byte[] initialVectorBytes = Encoding.ASCII.GetBytes(initialVector);
        byte[] saltValueBytes = Encoding.ASCII.GetBytes(salt);
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        PasswordDeriveBytes derivedPassword = new PasswordDeriveBytes(password, saltValueBytes, hashAlgorithm, passwordIterations);
        byte[] keyBytes = derivedPassword.GetBytes(keySize / 8);
        RijndaelManaged symmetricKey = new RijndaelManaged();
        symmetricKey.Mode = CipherMode.CBC;
        byte[] cipherTextBytes = null;
        using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initialVectorBytes))
        {
            using (MemoryStream memStream = new MemoryStream())
            {
                using (CryptoStream cryptoStream = new CryptoStream(memStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memStream.ToArray();
                    memStream.Close();
                    cryptoStream.Close();
                }
            }
        }
        symmetricKey.Clear();
        return Convert.ToBase64String(cipherTextBytes);
    }  /*********** КОНЕЦ ШИФРОВАНИЕ *******************/




    /****************** ДЕШИФРОВАНИЕ **************************/

    /// <summary> /// Decrypts a string /// </summary>
    ///Text to be decrypted
    ///Password to decrypt with
    ///Salt to decrypt with
    ///Can be either SHA1 or MD5
    ///Number of iterations to do
    ///Needs to be 16 ASCII characters long
    ///Can be 128, 192, or 256
    /// <returns>A decrypted string</returns>
    public static string Decrypt(string cipherText, string password, string salt, string hashAlgorithm, int passwordIterations, string initialVector, int keySize)
    {
        if (string.IsNullOrEmpty(cipherText))
            return "";
        byte[] initialVectorBytes = Encoding.ASCII.GetBytes(initialVector);
        byte[] saltValueBytes = Encoding.ASCII.GetBytes(salt);
        byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
        PasswordDeriveBytes derivedPassword = new PasswordDeriveBytes(password, saltValueBytes, hashAlgorithm, passwordIterations);
        byte[] keyBytes = derivedPassword.GetBytes(keySize / 8);
        RijndaelManaged symmetricKey = new RijndaelManaged();
        symmetricKey.Mode = CipherMode.CBC;
        byte[] plainTextBytes = new byte[cipherTextBytes.Length];
        int byteCount = 0;
        using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initialVectorBytes))
        {
            using (MemoryStream memStream = new MemoryStream(cipherTextBytes))
            {
                using (CryptoStream cryptoStream = new CryptoStream(memStream, decryptor, CryptoStreamMode.Read))
                {
                    byteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                    memStream.Close();
                    cryptoStream.Close();
                }
            }
        }
        symmetricKey.Clear();
        return Encoding.UTF8.GetString(plainTextBytes, 0, byteCount);
    }
}
