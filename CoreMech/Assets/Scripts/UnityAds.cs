﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections;
using System;

public class UnityAds : MonoBehaviour, IUnityAdsListener
{
#if UNITY_IOS
    public const string gameId = "4431984";
    public const string bannerPlacementID = "Banner_iOS";
    public const string interstitialPlacementID = "Interstitial_iOS";
    public const string rewardPlacementID = "Rewarded_iOS";
#elif UNITY_ANDROID
    public const string gameId = "4431985";
    public const string bannerPlacementID = "Banner_Android";
    public const string interstitialPlacementID = "Interstitial_Android";
    public const string rewardPlacementID = "Rewarded_Android";
#endif
    public static UnityAds Instance;
    public Button rewardedVideoAdsButton;
    public bool testMode = true;

    internal bool _wached;
    public delegate void Reward();
    public delegate void Skip();
    public delegate void Fail();

    public event Reward Rewarded;
    public event Skip Skiped;
    public event Fail Failed;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
        //Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, testMode);
        DontDestroyOnLoad(gameObject);
    }


    public void LoadAdinterstitial()
    {
        if (_wached) { _wached = false; return; }
        Debug.Log("Loading Ad: " + interstitialPlacementID);
        Advertisement.Load(interstitialPlacementID);
        StartCoroutine(IsLoaded(interstitialPlacementID));
    }

    public IEnumerator IsLoaded(string PlacementID)
    {
        while (!Advertisement.IsReady(PlacementID))
            yield return new WaitForSeconds(0.1f);
        ShowAd(PlacementID);
        Advertisement.AddListener(this);
    }

    private void ShowAd(string placementID)
    {
        Time.timeScale = 0;
        Debug.Log("Showing Ad: " + placementID);
        Advertisement.Show(placementID);
    }

    public void ShowAdinterstitial()
    {
        Debug.Log("Showing Ad: " + interstitialPlacementID);
        Advertisement.Show(interstitialPlacementID);
    }

    public void LoadRewardedVideo()
    {
        Debug.Log("Loading Ad: " + rewardPlacementID);
        Advertisement.AddListener(this);
        Advertisement.Load(rewardPlacementID);
        StartCoroutine(IsLoaded(rewardPlacementID));
    }

    private void ShowRewardedVideo()
    {
        Debug.Log("Showing Ad: " + rewardPlacementID);
        Advertisement.Show(rewardPlacementID);
    }

    public void OnUnityAdsDidFinish(string placementID, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {
            if(Rewarded != null)
            Rewarded();
        }
        else if (showResult == ShowResult.Skipped)
        {
            if (Skiped != null)
            Skiped();
        }
        else if (showResult == ShowResult.Failed)
        {
            if (Failed != null)
            Failed();
        }
        Time.timeScale = 1;
        Advertisement.RemoveListener(this);
    }

    public void OnUnityAdsReady(string placementId)
    {
        if (placementId == rewardPlacementID)
        {
            Advertisement.Show(rewardPlacementID);
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.LogError($"{message}");
    }

    public void OnUnityAdsDidStart(string placementID)
    {
        Debug.Log($"Add started at {placementID}");
    }

}
