﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewSkinsPool", menuName ="Scriptable Objects/SkinPool")]
public class SkinsPool : ScriptableObject
{
    public List<Skin> _skinList = new List<Skin>();
}

[System.Serializable]
public class Skin
{
    public string _name;
    public Material _skinMat;
    public int _skinCost;
    public Sprite _skinView;
}