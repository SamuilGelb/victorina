﻿using System;
using UnityEditor;
using UnityEngine;

public class LevelEditor : EditorWindow
{
    [SerializeField] DataModel model = new DataModel();
    private Vector2 _scrollPos;
    private bool _displayQuestions = false;
    int num;

    [MenuItem("Prototype/LevelEditor")]
    public static void ShowWindow()
    {
        GetWindow<LevelEditor>("Level editor");
    }

    public async void OnGUI()
    {
        GUILayout.Label($"Level <{model?.Num ?? '-'}>", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        GUILayout.Label($"Level {num}", EditorStyles.label);
        num = Convert.ToInt32(GUILayout.TextField($"{num}", EditorStyles.numberField));
        GUIStyle centeredBigBold = new GUIStyle();
        centeredBigBold.alignment = TextAnchor.MiddleCenter;
        centeredBigBold.fontSize = 18;
        centeredBigBold.fontStyle = FontStyle.Bold;
        GUILayout.EndHorizontal();
        GUILayout.Space(10);
        if (model == null)
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("CreateNewLevel"))
            {
                model = new DataModel();
                model.Num = num;
            }
            if (GUILayout.Button("Load"))
            {
                model = await DataModelGetter.GetData($"level_{num}");
            }
            GUILayout.EndHorizontal();
        }
        else
        {

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Clear"))
            {
                model = null;
                num = 0;
            }
            if (GUILayout.Button("Save"))
            {
                model.Num = num;
                DataModelGetter.SaveDataModel(model, $"level_{num}", ModelSource.Resources);
            }
            GUILayout.EndHorizontal();

            GUILayout.Label("Questions:", EditorStyles.largeLabel);
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Label($"Count: {model?.Questions?.Count ?? 0}", centeredBigBold, GUILayout.Height(40), GUILayout.Width(140));
            if (GUILayout.Button(_displayQuestions ? "/\\" : "\\/", GUILayout.Width(40), GUILayout.Height(40)))
            {
                _displayQuestions = !_displayQuestions;
            }
            GUILayout.EndHorizontal();
            if (_displayQuestions)
            {
                #region ScrollView
                _scrollPos = GUILayout.BeginScrollView(_scrollPos);
                int c = model?.Questions?.Count ?? 0;
                for (int i = 0; i < c; i++)
                {
                    var color = new Color32((byte)(160 + ((i % 2) * 16)), (byte)(160 + ((i % 2) * 16)), (byte)(160 + ((i % 2) * 16)), 255);
                    Texture2D bg = new Texture2D(1, 1);
                    bg.SetPixel(0, 0, color);
                    bg.Apply();
                    GUIStyle style = new GUIStyle();
                    style.normal.background = bg;
                    style.alignment = TextAnchor.MiddleCenter;

                    GUILayout.BeginHorizontal(style);
                    GUILayout.Label($"{i + 1}", centeredBigBold, GUILayout.Width(30), GUILayout.ExpandHeight(true));
                    GUILayout.BeginVertical();

                    GUILayout.Space(3);
                    GUILayout.Label("Question", EditorStyles.largeLabel);
                    GUILayout.Space(3);
                    //Question
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Question text: ", GUILayout.Width(105));
                    model.Questions[i].question.Text = GUILayout.TextField(model.Questions[i].question.Text, GUILayout.MaxWidth(395));
                    GUILayout.EndHorizontal();
                    //Question image name
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Resource name: ", GUILayout.Width(105));
                    model.Questions[i].question.ImageName = GUILayout.TextField(model.Questions[i].question.ImageName, GUILayout.MaxWidth(395));
                    GUILayout.EndHorizontal();
                    //Question image link
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Image URL: ", GUILayout.Width(105));
                    model.Questions[i].question.ImageURL = GUILayout.TextField(model.Questions[i].question.ImageURL, GUILayout.MaxWidth(395));
                    GUILayout.EndHorizontal();
                    //
                    GUILayout.Space(3);
                    GUILayout.Label("Answers", EditorStyles.largeLabel);
                    GUILayout.Space(3);
                    GUILayout.BeginHorizontal();
                    //Left
                    GUILayout.BeginVertical();
                    GUILayout.Label("Right Answer", EditorStyles.largeLabel);
                    GUILayout.Space(3);
                    //AnswerText
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Answer Text: ", GUILayout.Width(105));
                    model.Questions[i].RightAnswer.answer.Text = GUILayout.TextField(model.Questions[i].RightAnswer.answer.Text, GUILayout.MaxWidth(145));
                    GUILayout.EndHorizontal();
                    //Image URL
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Image URL: ", GUILayout.Width(105));
                    model.Questions[i].RightAnswer.answer.ImageURL = GUILayout.TextField(model.Questions[i].RightAnswer.answer.ImageURL, GUILayout.MaxWidth(145));
                    GUILayout.EndHorizontal();
                    //Image Name
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Resource Name: ", GUILayout.Width(105));
                    model.Questions[i].RightAnswer.answer.ImageName = GUILayout.TextField(model.Questions[i].RightAnswer.answer.ImageName, GUILayout.MaxWidth(145));
                    GUILayout.EndHorizontal();
                    model.Questions[i].RightAnswer.IsCorrect = true;
                    GUILayout.EndVertical();
                    //Space
                    GUILayout.BeginVertical(GUILayout.Width(10));
                    GUILayout.EndVertical();
                    //Right
                    GUILayout.BeginVertical();

                    GUILayout.Label("Wrong Answer", EditorStyles.largeLabel);
                    GUILayout.Space(3);
                    //AnswerText
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Answer Text: ", GUILayout.Width(105));
                    model.Questions[i].WrongAnswer.answer.Text = GUILayout.TextField(model.Questions[i].WrongAnswer.answer.Text, GUILayout.MaxWidth(145));
                    GUILayout.EndHorizontal();
                    //Image URL
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Image URL: ", GUILayout.Width(105));
                    model.Questions[i].WrongAnswer.answer.ImageURL = GUILayout.TextField(model.Questions[i].WrongAnswer.answer.ImageURL, GUILayout.MaxWidth(145));
                    GUILayout.EndHorizontal();
                    //Image Name
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Resource Name: ", GUILayout.Width(105));
                    model.Questions[i].WrongAnswer.answer.ImageName = GUILayout.TextField(model.Questions[i].WrongAnswer.answer.ImageName, GUILayout.MaxWidth(145));
                    GUILayout.EndHorizontal();
                    model.Questions[i].WrongAnswer.IsCorrect = false;
                    GUILayout.EndVertical();
                    GUILayout.EndHorizontal();

                    GUILayout.Space(5);
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Reward", EditorStyles.largeLabel, GUILayout.Width(75));
                    model.Questions[i].Rewarded = GUILayout.Toggle(model.Questions[i].Rewarded, "is rewarded");
                    GUILayout.EndHorizontal();
                    //Reward
                    if (model.Questions[i].Rewarded)
                    {
                        GUILayout.Space(3);
                        //Money
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Money: ", GUILayout.Width(105));
                        model.Questions[i].QuestionReward.Money = Convert.ToInt32(GUILayout.TextField($"{model.Questions[i].QuestionReward.Money}", EditorStyles.numberField, GUILayout.MaxWidth(395)));
                        GUILayout.EndHorizontal();
                        //Money
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Hint: ", GUILayout.Width(105));
                        model.Questions[i].QuestionReward.Hint = Convert.ToInt32(GUILayout.TextField($"{model.Questions[i].QuestionReward.Hint}", EditorStyles.numberField, GUILayout.MaxWidth(395)));
                        GUILayout.EndHorizontal();
                        //Money
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Rating: ", GUILayout.Width(105));
                        model.Questions[i].QuestionReward.Rating = Convert.ToInt32(GUILayout.TextField($"{model.Questions[i].QuestionReward.Rating}", EditorStyles.numberField, GUILayout.MaxWidth(395)));
                        GUILayout.EndHorizontal();
                        //Money
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Skin: ", GUILayout.Width(105));
                        model.Questions[i].QuestionReward.Skin = GUILayout.TextField(model.Questions[i].QuestionReward.Skin);
                        GUILayout.EndHorizontal();
                    }
                    //
                    GUILayout.EndVertical();
                    if (GUILayout.Button("X", GUILayout.Width(50), GUILayout.ExpandHeight(true)))
                        model.Questions.RemoveAt(i);
                    GUILayout.EndHorizontal();
                }
                if (GUILayout.Button("+", EditorStyles.miniButtonRight))
                {
                    model.Questions.Add(new Question());
                }
                GUILayout.EndScrollView();
                #endregion
            }
            //Reward
            GUILayout.Space(5);
            GUILayout.Label("Reward", EditorStyles.largeLabel, GUILayout.Width(75));
            GUILayout.Space(3);
            //Money
            GUILayout.BeginHorizontal();
            GUILayout.Label("Money: ", GUILayout.Width(105));
            model.Reward.Money = Convert.ToInt32(GUILayout.TextField($"{model.Reward.Money}", EditorStyles.numberField, GUILayout.MaxWidth(395)));
            GUILayout.EndHorizontal();
            //Money
            GUILayout.BeginHorizontal();
            GUILayout.Label("Hint: ", GUILayout.Width(105));
            model.Reward.Hint = Convert.ToInt32(GUILayout.TextField($"{model.Reward.Hint}", EditorStyles.numberField, GUILayout.MaxWidth(395)));
            GUILayout.EndHorizontal();
            //Money
            GUILayout.BeginHorizontal();
            GUILayout.Label("Rating: ", GUILayout.Width(105));
            model.Reward.Rating = Convert.ToInt32(GUILayout.TextField($"{model.Reward.Rating}", EditorStyles.numberField, GUILayout.MaxWidth(395)));
            GUILayout.EndHorizontal();
            //Money
            GUILayout.BeginHorizontal();
            GUILayout.Label("Skin: ", GUILayout.Width(105));
            model.Reward.Skin = GUILayout.TextField(model.Reward.Skin, GUILayout.MaxWidth(250));
            GUILayout.EndHorizontal();

        }
    }
}
