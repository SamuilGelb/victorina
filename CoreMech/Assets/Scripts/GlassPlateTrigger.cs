﻿using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class GlassPlateTrigger : MonoBehaviour
{
    private GlassPlateController _controller;

    public void Awake()
    {
        _controller = transform.GetComponentInParent<GlassPlateController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player" || other.transform.tag == "Bot")
        {
            if(!_controller.DoCheck(other.transform.tag == "Player"))
            {
                other.GetComponentsInChildren<SoundTrigger>()
                    .ToList().First(x => x.Tag == "Scream").PlayRandom();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            Debug.Log("Pass");
        }
    }
}
