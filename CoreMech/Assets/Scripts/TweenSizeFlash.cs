﻿using UnityEngine;

public class TweenSizeFlash : MonoBehaviour
{
    [SerializeField] private bool _activeOnEnable = false;
    [SerializeField] private float _minValue = 0.5f;
    [SerializeField] private float _maxValue = 1.5f;
    [SerializeField] private float _loopTime = 1f;
    [SerializeField] private bool _loop;

    public void OnEnable()
    {
        if (_activeOnEnable)
            Flash();
    }

    private void Flash()
    {
        LeanTween.size(transform as RectTransform, Vector2.one * _minValue, _loopTime).setOnComplete(() =>
        {
            LeanTween.size(transform as RectTransform, Vector2.one * _maxValue, _loopTime).setOnComplete(() =>
            {
                if (_loop)
                    Flash();
            });
        });
    }
}
