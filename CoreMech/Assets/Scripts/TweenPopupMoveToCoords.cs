﻿using UnityEngine;
using UnityEngine.UI;

public class TweenPopupMoveToCoords : BaseTweenPopupController
{
    public Vector2 _cordsToMove;
    public Vector2 _shiftOffset;
    public float _timeToColor;
    public float _timeToDisplay;
    public float _timeOnShift;

    public override void Show()
    {
        MainPanel.gameObject.SetActive(true);
        PopupItem.anchoredPosition = new Vector2(0, -Screen.height);
        var mainImg = MainPanel.GetComponent<Image>();
        LeanTween.value(gameObject, mainImg.color, new Color(0, 0, 0, 0.35f), _timeToColor)
        .setOnUpdate((Color col) =>
        {
            mainImg.color = col;
        })
        .setOnComplete(() =>
        {
            LeanTween.value(gameObject, new Vector2(0, -Screen.height), _shiftOffset, _timeToDisplay - _timeOnShift)
            .setOnUpdate((Vector2 v) =>
            {
                PopupItem.anchoredPosition = v;
            })
            .setOnComplete(() =>
            {
                LeanTween.value(gameObject, _shiftOffset, _cordsToMove, _timeOnShift)
                .setOnUpdate((Vector2 v) =>
                {
                    PopupItem.anchoredPosition = v;
                })
                .setOnComplete(() =>
                {
                    OnViewChanged(true);
                });
            });
        });
    }

    public override void Hide(bool fast = false)
    {
        if (fast)
            base.Hide(fast);
        else
        {
            LeanTween.value(gameObject, _cordsToMove, _shiftOffset, _timeOnShift / 2)
            .setOnUpdate((Vector2 v) =>
            {
                PopupItem.anchoredPosition = v;
            })
            .setOnComplete(() =>
            {
                LeanTween.value(gameObject, _shiftOffset, new Vector2(0, -Screen.height),
                (_timeToDisplay - _timeOnShift) / 2)
                .setOnUpdate((Vector2 v) =>
                {
                    PopupItem.anchoredPosition = v;
                })
                .setOnComplete(() =>
                {
                    var mainImg = MainPanel.GetComponent<Image>();
                    LeanTween.value(gameObject, mainImg.color, new Color(0, 0, 0, 0), _timeToColor / 2)
                    .setOnUpdate((Color col) =>
                    {
                        mainImg.color = col;
                    })
                    .setOnComplete(() =>
                    {
                        OnViewChanged(false);
                        MainPanel.gameObject.SetActive(false);
                    });
                });
            });
        }
    }
}
