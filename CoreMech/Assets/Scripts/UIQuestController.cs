﻿using TMPro;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestController : BaseCanvasController
{
    [Header("Question")]
    public RectTransform _questionHolder;
    public TextMeshProUGUI _questionText;
    public Image _questionImage;
    [Header("Buttons")]
    public RectTransform _buttonsGroup;
    public Button _leftButton;
    public Button _rightButton;

    [SerializeField] private Question _question;
    public Action<Side> _onSelect = delegate (Side side) { };

    private new void Awake()
    {
        base.Awake();
        Subscribe();
    }

    public void Subscribe()
    {
        _leftButton.onClick.AddListener(() => OnSelect(Side.Left));
        _rightButton.onClick.AddListener(() => OnSelect(Side.Right));
    }

    public void DisplayQuest(Question question, Action<Side> action)
    {
        Show();
        _question = question;
        _onSelect = action;
        _questionImage.gameObject.SetActive(false);
        var imageSource = DataModelUtils.ContainImage(question.question);
        _questionText.rectTransform.sizeDelta = new Vector2(900, 450);
        if (imageSource != ImageSource.Null)
        {
            _questionImage.gameObject.SetActive(true);
            _questionText.rectTransform.sizeDelta = new Vector2(450, 450);
            _questionImage.rectTransform.sizeDelta = new Vector2(450, 450);
            switch (imageSource)
            {
                case ImageSource.Link:
                    StartCoroutine(DataModelUtils.DownloadImage(_question.question.ImageURL, _questionImage));
                    break;
                case ImageSource.Resources:
                    _questionImage.sprite = DataModelUtils.GetImage(_question.question.ImageName);
                    break;
            }
        }
        _questionText.text = _question.question.Text;
    }

    private void OnSelect(Side side)
    {
        _buttonsGroup.gameObject.SetActive(false);
        Hide();
        _onSelect(side);
    }

    public override void Show()
    {
        if (_isActive) return;
        base.Show();
        _buttonsGroup.gameObject.SetActive(true);
        LeanTween.value(-_questionHolder.sizeDelta.y, 100, 0.5f).setOnUpdate(
            (float z) =>
            {
                _questionHolder.anchoredPosition = new Vector2(0, z);
            });
    }

    public override void Hide(bool fast = false)
    {
        if (fast)
        {
            base.Hide();
            _questionHolder.anchoredPosition = new Vector2(0, -_questionHolder.sizeDelta.y);
        }
        else
        {
            LeanTween.value(100, -_questionHolder.sizeDelta.y, 0.5f).setOnUpdate(
                (float z) =>
                {
                    _questionHolder.anchoredPosition = new Vector2(0, z);
                });
        }
        _isActive = false;
        _buttonsGroup.gameObject.SetActive(false);
    }
}

public enum Side
{
    Left,
    Right
}